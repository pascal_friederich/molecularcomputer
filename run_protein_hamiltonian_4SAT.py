#!/usr/bin/env python
# This code was written by Pascal Friederich 

import os
import sys
import numpy as np
import time
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt 
import droplet_computer.functions as dcf
import yaml

from get_coefficients_4SAT import read_Hs_11_non_reduced



def read_protein_hamiltonian():
    Hs,ground_state_energy,n=read_Hs_11_non_reduced()
    return(Hs,ground_state_energy,n)


def state_to_protein(state,n_physical=7):
    state_full=[0,1,0]+(state).astype(np.int64).tolist()[:n_physical]
    directions=[]
    positions=[[0,0]]
    for i in range(len(state_full)//2):
        start=i*2
        x=state_full[start]
        y=state_full[start+1]
        if x==0 and y==0:
            directions.append("down")
            position_new=[positions[-1][0],positions[-1][1]-1]
            positions.append(position_new)
        elif x==0 and y==1:
            directions.append("right")
            position_new=[positions[-1][0]+1,positions[-1][1]]
            positions.append(position_new)
        elif x==1 and y==0:
            directions.append("left")
            position_new=[positions[-1][0]-1,positions[-1][1]]
            positions.append(position_new)
        elif x==1 and y==1:
            directions.append("up")
            position_new=[positions[-1][0],positions[-1][1]+1]
            positions.append(position_new)

    return(state_full,directions,positions)



def plot(sequence,positions,filename):
    fig,axes=plt.subplots(1,1)
    extreme=len(sequence)
    for idx,pos in enumerate(positions):
        x=pos[0]
        y=pos[1]
        circle = plt.Circle((x, y), 0.2,facecolor="white",edgecolor="k",zorder=5)

        axes.add_artist(circle)
        if abs(x)>extreme:
            extreme=abs(x)
        if abs(y)>extreme:
            extreme=abs(y)
    
        plt.text(pos[0]-0.03,pos[1]-0.05,sequence[idx],zorder=10,fontsize=4)
        if idx>0:
            x_old=positions[idx-1][0]
            y_old=positions[idx-1][1]
            plt.plot([x_old,x],[y_old,y],"k-",zorder=2)
    plt.xlim([-extreme-1,extreme+1])
    plt.ylim([-extreme-1,extreme+1])
    plt.xticks([])
    plt.yticks([])
    plt.savefig(filename,dpi=300)
    plt.close()


def transform_2SAT_to_11_hamiltonian(Hs):
    H0=Hs[0]
    H1=Hs[1]
    H2=Hs[2]
    # sum of diagonal elements+sum of all off diagonal elements
    H0_new=H0+0.5*np.sum(np.copy(H1))+0.25*(np.sum(np.copy(H2)))
    # sum of diagonal elements
    H1_new=0.5*np.copy(H1)+0.5*np.sum(H2,axis=1)
    # sum of off-diagonal elements
    H2_new=0.25*np.copy(H2)
    return(H0_new,H1_new,H2_new)


def transform_3SAT_to_11_hamiltonian(Hs):
    H0=Hs[0]
    H1=Hs[1]
    H2=Hs[2]
    H3=Hs[3]
    # rank 0
    H0_new=                        H0+1.0/(2.0**3.0)*np.sum(H1,axis=0)*4.0+1.0/(2.0**3.0)*np.sum(H2,axis=(0,1))*2.0*1.0+1.0/(2.0**3.0)*np.sum(H3,axis=(0,1,2))
    # rank 1
    H1_new=1.0/(2.0**3.0)*np.copy(H1)*4.0+1.0/(2.0**3.0)*np.sum(H2,axis=(1))*1.0*4.0+1.0/(2.0**3.0)*np.sum(H3,axis=(1,2))*3.0
    # rank 2
    H2_new=1.0/(2.0**3.0)*np.copy(H2)*1.0*2.0+1.0/(2.0**3.0)*np.sum(H3,axis=2)*3.0
    # rank 3
    H3_new=1.0/(2.0**3.0)*np.copy(H3)
    return(H0_new,H1_new,H2_new,H3_new)


def transform_4SAT_to_11_hamiltonian(Hs):
    H0=Hs[0]
    H1=Hs[1]
    H2=Hs[2]
    H3=Hs[3]
    H4=Hs[4]
    # rank 4
    H4_new=1.0/(2.0**4.0)*np.copy(H4)
    # rank 3
    H3_new=1.0/(2.0**4.0)*np.sum(H4,axis=3)*4.0   +   1.0/(2.0**4.0)*np.copy(H3)*1.0*2.0
    # rank 2
    H2_new=1.0/(2.0**4.0)*np.sum(H4,axis=(2,3))*6.0   +   1.0/(2.0**4.0)*np.sum(H3,axis=2)*1.0*6.0   +   1.0/(2.0**4.0)*np.copy(H2)*1.0*4.0
    # rank 1
    H1_new=1.0/(2.0**4.0)*np.sum(H4,axis=(1,2,3))*4.0   +   1.0/(2.0**4.0)*np.sum(H3,axis=(1,2))*1.0*6.0   +   1.0/(2.0**4.0)*np.sum(H2,axis=1)*1.0*8.0   +   1.0/(2.0**4.0)*np.copy(H1)*8.0
    # rank 0
    H0_new=1.0/(2.0**4.0)*(np.sum(np.copy(H4)))   +   1.0/(2.0**4.0)*(np.sum(np.copy(H3)))*1.0*2.0   +   1.0/(2.0**4.0)*(np.sum(np.copy(H2)))*1.0*4.0   +   1.0/(2.0**4.0)*np.sum(np.copy(H1))*8.0   +   H0
    return(H0_new,H1_new,H2_new,H3_new,H4_new)



def plot_energy(energies,trajectory,mean_dstates,settings):
    '''
    fig,axis=plt.subplots(2,1,sharex=True)
    axis[0].set_yscale("log")
    axis[1].set_yscale("log")
    axis[0].plot(range(len(energies)),np.array(energies)+7.0,"k.")
    axis[0].plot(range(len(energies)),7.0*np.ones((len(energies))),"k--")
    axis[1].plot(range(len(energies)),mean_dstates,"k.")
    for idx,e in enumerate(energies):
        if (idx+1)%5000==0 and idx>0:
            plt.text(idx,2.0,"%i"%(energies[idx]))
    #axis[0].set_xticks([])
    axis[1].set_xlabel("Stepnumber")
    axis[0].set_ylabel("Energy [a.u.]")
    axis[1].set_ylabel("Gradient [a.u.]")
    axis[0].set_ylim([1e0,1e4])
    axis[1].set_ylim([0.0,1.1*np.max(mean_dstates)])
    plt.savefig("energy_over_stepnumber.png",dpi=300)
    plt.close()
    '''
    fig,axes=plt.subplots(3,1,sharex=True)
    steps=np.array(range(len(energies)))

    axes[0].plot(steps,energies,"k-")
    axes[0].plot([0.0,steps[-1]],[0.0,0.0],"k-")
    axes[0].plot([0.0,steps[-1]],[-6.0,-6.0],"k--")
    axes[0].set_xlabel("Stepnumber")
    axes[0].set_ylabel("Energy")
    axes[0].set_ylim([-7.0,10.0])

    axes[1].plot(steps,trajectory["temperatures"],"k-")
    axes[1].plot([0.0,steps[-1]],[settings["temperature"],settings["temperature"]],"k-")
    axes[1].plot([0.0,steps[-1]],[0.0,0.0],"k-")
    axes[1].set_ylabel("Temperature")
    axes[1].set_ylim([0.0,1.5*settings["temperature"]])

    axes[2].plot(steps,trajectory["Ss"],"k-")
    axes[2].plot([0.0,steps[-1]],[0.0,0.0],"k-")
    axes[2].set_ylabel("Nose-Hoover\nparameter S")
    #axes[2].set_ylim([-10.0,10.0])
    plt.subplots_adjust(hspace=0.0)
    plt.savefig("energy_over_stepnumber.png")
    plt.close()

def check_second_half(state_here):
    

    # check the 4to2 mapping terms:
    state_here_10=0.5*(state_here+1.0)
    okay=True
    #maps=[[2,4,20],[1,3,21],[3,5,22],[1,5,23],[2,6,24],[4,6,25],[3,7,26],[5,7,27],[1,7,28]]
    #for m in maps:
    #    if state_here_10[m[0]-1]*state_here_10[m[1]-1]!=state_here_10[m[2]-1]:
    #        okay=False
    #        break
    #if not okay:
    #    return(okay)

    # check bindings
    full_state,directions,positions=state_to_protein(state_here_10)

    bindings=[["P","K",-1],["P","A",-2],["S","M",-3],["V","A",-4]]
    sequence=["P","S","V","K","M","A"]
    bindings=[[0,3,-1],[0,5,-2],[1,4,-3],[2,5,-4]]
    positions=np.array(positions)
    for bidx, b in enumerate(bindings):
        state=state_here_10[15+bidx]
        dist=np.linalg.norm(positions[b[0]]-positions[b[1]])
        if abs(dist-1.0)<1e-4 and state==0:
            return(False)
        elif dist>1.001 and state==1:
            return(False)
    return(okay)
      


def state_11_to_float_string(state_11):
    state_01=0.5*(1.0+state_11)
    state_string_1="["
    for s in state_01[:7]:
        if abs(s)<1e-3:
            state_string_1+="0 "
        elif abs(s-1.0)<1e-3:
            state_string_1+="1 "
        else:
            state_string_1+="%.3f "%(s)
    state_string_1+="]"

    state_string_2="["
    for s in state_01[7:15]:
        if abs(s)<1e-3:
            state_string_2+="0 "
        elif abs(s-1.0)<1e-3:
            state_string_2+="1 "
        else:
            state_string_2+="%.3f "%(s)
    state_string_2+="]"

    state_string_3="["
    for s in state_01[15:]:
        if abs(s)<1e-3:
            state_string_3+="0 "
        elif abs(s-1.0)<1e-3:
            state_string_3+="1 "
        else:
            state_string_3+="%.3f "%(s)
    state_string_3+="]"
    return(state_string_1,state_string_2,state_string_3)


if __name__== "__main__":


    settings={"n":3, # number of droplets
              "X":3,
              "SAT":"1 or 2 , 2 or not3", # the SAT problem, everything is combined with ANDs, or "random"
              "num_clauses":30, # number of clauses of length X to generate if SAT=="random"
              "steps":10000, # number of steps per annealing cycle, should be 1000 in repeat mode
              "restarts":10000, # number of independent trajectories to do in repeat mode
              "stepsize":0.1, # step size
              "noise":0.1, # noise in trajectory mode
              "plot":"off", # plot can be live, normal, off
              "plot_freq":30, # after how many steps do we need a plot
              "debug":False, # much more output
              "check_all_states": False, # generate a list of all possible states
              "mode":"trajectory", # mode can be trajectory (restarts is set to 1) or repeat (noise is set to 0)
              "nearest_neighbors_only": False,
              "membrane": "linear", # linear or quadratic
              "stop_at_first_hit": False,
              "silent": False
              }

    # write the default settings to a file if the file does not exist yet
    if not os.path.exists("settings.yml"):
        print("No settings file found. Default values will be written to settings.yml. Please check the settings and restart the calculation.")
        outfile=open("settings_4SAT.yml","w")
        for x in settings:
            outfile.write("%s: %s\n"%(str(x),str(settings[x])))
        outfile.close()


    # load the settings
    settings=yaml.load(open("settings_4SAT.yml","r"))

    Hs_11, ground_state_energy, n = read_protein_hamiltonian()
    if settings["n"]!=n:
        print("WARNING: THE n (%i) GIVEN IN settings.yml DOES NOT FIT TO THE HAMILTONIAN (%i)"%(settings["n"],n))
        settings["n"]=n
    print("Energy while reading mathematica output: %.2f"%(ground_state_energy))

    sequence=["P","S","V","K","M","A"]
    sequence=["1","2","3","4","5","6","7"]
    n_physical=9

    if n==19:
        state_folded_01=np.array([0,0,0,1,0,1,1,1,1,0,0,1,1,1,0,0,1,0,1]) # new ground state by Douglas
        #energy_folded_01=dcf.get_energy_non_reduced(Hs_01,state_folded_01,settings)
        #print("Energy in 0/1 basis: %.2f"%(energy_folded_01))


        #Hs_11=transform_4SAT_to_11_hamiltonian(Hs_01)
        state_folded_11=np.copy(state_folded_01)*2.0-1.0
        energy_folded_11=dcf.get_energy_non_reduced(Hs_11,state_folded_11,settings)
        print("Energy in -1/1 basis: %.2f"%(energy_folded_11))


        full_conformer_folded,directions_folded,positions_folded=state_to_protein(0.5*(state_folded_11+1.0),n_physical=7)

        plot(sequence,positions_folded,"plots_4body/protein_folded.png")


    '''
    ######### ALL STATES
    n7=7
    num7=2**n7
    print("generate %i states for directions"%(num7))
    all_states_7=[]
    for i in range(num7):
        state_here=[]
        state_here_string="{0:b}".format(i)
        if len(state_here_string)<n7:
            state_here_string="%s%s"%("0"*(n7-len(state_here_string)),state_here_string)
        for char in state_here_string:
            if char=="0":
                state_here.append(-1)
            elif char=="1":
                state_here.append(1)
        all_states_7.append(state_here)

    n12=12
    num12=2**n12
    print("generate %i states for ancillas"%(num12))
    all_states_12=[]
    for i in range(num12):
        state_here=[]
        state_here_string="{0:b}".format(i)
        if len(state_here_string)<n12:
            state_here_string="%s%s"%("0"*(n12-len(state_here_string)),state_here_string)
        for char in state_here_string:
            if char=="0":
                state_here.append(-1)
            elif char=="1":
                state_here.append(1)
        all_states_12.append(state_here)

    print("start with energy evaluations")

    all_energies=[]
    all_directions=[]
    for i in range(num7):
        energy_here=1e10
        full_state_here,directions_here,positions_here=state_to_protein(0.5*(np.array(all_states_7[i]+all_states_12[0])+1.0))
        print("get minimal energy for %i out of %i"%(i+1,num7))
        state_01_here=""
        for j in range(num12):
            state_to_check=np.array(all_states_7[i]+all_states_12[j])

            valid=check_second_half(state_to_check)
            if valid:

                energy=dcf.get_energy_non_reduced(Hs_11,state_to_check,settings)
                if energy<energy_here:
                    state_01=(state_to_check+1.0)/2.0
                    energy_here=energy
                    state_01_here=state_01
                print("%s, try %i out of %i: minimum so far: %.0f"%(directions_here,j+1,num12,energy_here))
        all_energies.append(energy_here)
        all_directions.append(directions_here)


        if state_01_here != "":
            state_string="%i%i%i%i%i%i%i"%(state_01_here[0],state_01_here[1],state_01_here[2],state_01_here[3],state_01_here[4],state_01_here[5],state_01_here[6])
            print(directions_here,energy_here, "[ %s ]"%(state_string))
            plot(sequence,positions_here,"plots_all/protein_%s_energy_%i.png"%(state_string, int(round(energy_here))))
            outfile=open("plots_all/protein_%s_energy_%i.dat"%(state_string, int(round(energy_here))),"w")
            for x in state_01_here:
                outfile.write("%i "%(int(round(x))))
            outfile.write("\n")
            outfile.close()


    exit()
    '''

    



    state_11=np.random.random((settings["n"]))*2.0-1.0
    #state_11=np.array([0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0 ])*2.0-1.0 # E = -3 state
    #state_11=np.array([0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0 ])*2.0-1.0 # E = -3 state
    fixed_indeces=[0,2,3,4,5]
    fixed_indeces=[]
    fixed_states=[0,0,1,0,1]
    for idx,index in enumerate(fixed_indeces):
        state_11[index]=float(fixed_states[idx])*2.0-1.0

    #dstate,noise=dcf.do_one_step_protein_non_reduced_MD(np.copy(state_11),Hs_11,settings)



    state_11_int=np.array(dcf.get_state_from_conc(state_11))
    full_conformer_start,directions_start,positions_start=state_to_protein(0.5*(state_11_int+1.0),n_physical=n_physical)
    plot(sequence,positions_start,"plots_4body/protein_start.png")
    print(directions_start)
    #state=np.zeros((settings["n"]))
    #state=np.copy(state_folded)

    energies=[]
    energies_rounded=[]
    directions=[]
    mean_dstates=[]
    trajectory={"momenta":[],"forces":[],"Ss":[],"temperatures":[]}
    #counter=0
    for step in range(settings["steps"]):
    #for step in range(50):

        # some debug output
        #print("   ---   Step %i out of %i"%(step+1,settings["steps"]))

        if step%50==0:
            if step>0:
                plot_energy(energies,trajectory,mean_dstates,settings)

                state_11_int=np.array(dcf.get_state_from_conc(state_11))
                #full_conformer_here,directions_here,positions_here=state_to_protein(0.5*(state_11_int+1.0),n_physical=n_physical)
                #plot(sequence,positions_here,"plots_4body/protein_step_%05d_energy_%i.png"%(step,int(round(energy))))
                if step%100000==0:
                    print("start a new conformation")
                    state_11=np.random.random((settings["n"]))*2.0-1.0
        # do one time step
        noise=dcf.do_one_step_protein_non_reduced_MD(state_11,Hs_11,trajectory,settings,fixed_indeces=fixed_indeces)
        mean_dstate=np.mean(np.abs(trajectory["forces"][-1]))
        mean_dstates.append(mean_dstate)

        # get energies
        energy=dcf.get_energy_non_reduced(Hs_11,state_11,settings)
        energy_rounded=int(round(energy))
        energies.append(energy)
        energies_rounded.append(energy_rounded)


        state_11_int=np.array(dcf.get_state_from_conc(state_11))
        state_01=0.5*(state_11+1.0)
        full_conformer_here,directions_here,positions_here=state_to_protein(0.5*(state_11_int+1.0),n_physical=n_physical)
        state_10_int=(state_11_int+1)//2
        directions.append(directions_here)
        energy_rounded_state=int(round(dcf.get_energy_non_reduced(Hs_11,state_11_int.astype(np.float32),settings)))

        s1,s2,s3=state_11_to_float_string(state_11)

        print(step,directions_here,s1,s2,s3,"%.3f"%(energy),energy_rounded_state,"gradient: %.4f"%(mean_dstate))

        if step>2 and directions_here!=directions[-2]:
            
            plot(sequence,positions_here,"plots_4body/protein_step_%05d_energy_%i.png"%(step,int(round(energy))))
            #counter+=1
            #print(full_state_here)

    plot_energy(energies,trajectory,mean_dstates,settings)

    state_11_int=np.array(dcf.get_state_from_conc(state_11))
    full_state_end,directions_end,positions_end=state_to_protein(0.5*(state_11_int+1.0),n_physical=n_physical)
    plot(sequence,positions_end,"plots_4body/protein_end.png")

