#!/usr/bin/env python
# This code was written by Pascal Friederich in collaboration with Si Yue Guo and Tony Wu in 2018

import os
import sys
import time
import subprocess
import shlex
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import droplet_computer.functions as dcf
import droplet_computer.hamiltonians as dch
import droplet_computer.plot_functions as dcp
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import yaml


def run_droplet_computer(**kwargs):
    # default settings
    settings={"n":3, # number of droplets
              "X":3,
              "SAT":"1 or 2 , 2 or not3", # the SAT problem, everything is combined with ANDs, or "random"
              "num_clauses":30, # number of clauses of length X to generate if SAT=="random"
              "steps":10000, # number of steps per annealing cycle, should be 1000 in repeat mode
              "restarts":10000, # number of independent trajectories to do in repeat mode
              "stepsize":0.1, # step size
              "noise":0.1, # noise in trajectory mode
              "plot":"off", # plot can be live, normal, off
              "plot_freq":30, # after how many steps do we need a plot
              "debug":False, # much more output
              "check_all_states": False, # generate a list of all possible states
              "mode":"trajectory", # mode can be trajectory (restarts is set to 1) or repeat (noise is set to 0)
              "nearest_neighbors_only": False,
              "membrane": "linear", # linear or quadratic
              "stop_at_first_hit": False,
              "silent": False
              }

    # write the default settings to a file if the file does not exist yet
    if not os.path.exists("settings.yml"):
        print("No settings file found. Default values will be written to settings.yml. Please check the settings and restart the calculation.")
        outfile=open("settings.yml","w")
        for x in settings:
            outfile.write("%s: %s\n"%(str(x),str(settings[x])))
        outfile.close()


    # load the settings
    settings=yaml.load(open("settings.yml","r"))
    silent=settings["silent"]
    for key, value in kwargs.items():
        if not silent:
            print("overwrite settings.%s with %s"%(key,value))
        settings[key]=value


    # some settings have to be overwritten depending on which mode to use
    #if settings["mode"]=="trajectory":
    #    settings["restarts"]=1
    #elif settings["mode"]=="repeat":
    #    settings["noise"]=0.0
    #    if settings["steps"]>1000:
    #        print("WARNING: %i STEPS IN REPEAT MODE WILL TAKE VERY LONG. 1000 STEPS SHOULD BE ENOUGH."%(settings["steps"]))

    # number of sites
    n=settings["n"] # x dimension of the lattice

    # X-SAT
    X=settings["X"]

    # forget about this, it will only make the droplets in the video move a bit
    plot_settings=np.random.random((n,5,100))

    # SAT
    if settings["SAT"]=="random":
        num_clauses=settings["num_clauses"]
        clauses=[]
        for i in range(num_clauses):
            if i<n:
                index_must=i
            else:
                index_must=None
            found=False
            while not found:
                new_clause=dcf.generate_random_clause(n,X,index_must=index_must)
                if new_clause not in clauses:
                    found=True
                    clauses.append(new_clause)
        SAT=dcf.sat_from_clauses(clauses)
        settings["SAT"]=SAT
    else:
        num_clauses=len(settings["SAT"].split(","))

    #settings["SAT"]="1 or not4 or 9 , 2 or 9 or not29 , not1 or 3 or not25 , 4 or 23 or 24 , not5 or not15 or 16 , not2 or not6 or 26 , 2 or not7 or 22 , 1 or 6 or not8 , 9 or 16 or not19 , 3 or not10 or not16 , not5 or 11 or 28 , not10 or 11 or 12 , not1 or not13 or 29 , 3 or 14 or not22 , 8 or 11 or not15 , 1 or not16 or not26 , not12 or not17 or not18 , 1 or not18 or not20 , not16 or not19 or 21 , 2 or 14 or 20 , 15 or not21 or 22 , not7 or 18 or not22 , 11 or not13 or not23 , 9 or not24 or not25 , 20 or not25 or not28 , not1 or not19 or 26 , not7 or 15 or not27 , not11 or 28 or not29 , 24 or not26 or not29 , 7 or not29 or 30 , 17 or not19 or 26 , 3 or not6 or not14 , not2 or 7 or not16 , 2 or not11 or not13 , 12 or 20 or 24 , 11 or not21 or 25 , not7 or 20 or 30 , not12 or not16 or not22 , not4 or not5 or not9 , 7 or not19 or 20 , 5 or 21 or 25 , not9 or not14 or 18 , not6 or not14 or 23 , 2 or 17 or 26 , not17 or not21 or not27 , not6 or 9 or 28 , not3 or 10 or 11 , not8 or 9 or not15 , 2 or not5 or not30 , 2 or not5 or not22 , 2 or not9 or 15 , not1 or not4 or 9 , 9 or not18 or not24 , 19 or not23 or not29 , not12 or 17 or 22 , 1 or 7 or 15 , not13 or 21 or 27 , 7 or not10 or not24 , not3 or 11 or not28 , not1 or 21 or 26 , not7 or 15 or not16 , not5 or 17 or not22 , 4 or not6 or not26 , not3 or not20 or 30 , 8 or 15 or not30 , 8 or not22 or 29 , 14 or not26 or 29 , not5 or 9 or not24 , 5 or 8 or not25 , 1 or not14 or not15 , not7 or not22 or not29 , not3 or 14 or 22 , 24 or not28 or 30 , 2 or 4 or 22 , 4 or not7 or 19 , 3 or 11 or not19 , not16 or 25 or not27 , 1 or not23 or not29 , not6 or 17 or 23 , 5 or not8 or not25 , not3 or 5 or 12 , not10 or not17 or 29 , not21 or 23 or not27 , not14 or 18 or 28 , 13 or 21 or not28 , 5 or 14 or 25 , not3 or not14 or not18 , not2 or not22 or 24 , not17 or not18 or 25 , not5 or 7 or not28 , not4 or not10 or 20 , not2 or 3 or 13 , 9 or 25 or not28 , not12 or not17 or 22 , 2 or not15 or not28 , 10 or not22 or not28 , not6 or 25 or not30 , 2 or 14 or not18 , not9 or 10 or 12 , 3 or 5 or 14 , not6 or 18 or 24 , not10 or 19 or not22 , not1 or 7 or not23 , not14 or 20 or not30 , not8 or 11 or not15 , not3 or 7 or not29 , 3 or 6 or not9 , not9 or not16 or 17 , 10 or not15 or not29 , 4 or not6 or not11 , 11 or 14 or not21 , not1 or 6 or 20 , not1 or 2 or 20 , 22 or not24 or not29 , 2 or 24 or 29 , not5 or 17 or not18 , 7 or 18 or 22 , not1 or not14 or 23 , not1 or 10 or 23 , not22 or not26 or 27"


    # DPLL
    symbol_list = []
    for i in range(26):
        symbol_list.append(chr(i + 65))
    for i in range(26):
        symbol_list.append(chr(i + 97))
    random_string=str(abs(np.random.randint(1e10)))
    outfile=open("sat_%s.lisp"%(random_string),"w")
    outfile.write("(and")
    for c in clauses:
        outfile.write(" (or")
        ints=[]
        nots=[]
        for x in c.split():
            if "or" in x:
                continue
            elif "not" in x:
                nots.append("!")
                ints.append(x.replace("not",""))
                i=x.replace("not","")
                outfile.write(" (not %s)"%(symbol_list[int(i)-1]))
            else:
                nots.append("")
                ints.append(x)
                i=x
                outfile.write(" %s"%(symbol_list[int(i)-1]))
        outfile.write(")")
    outfile.write(")\n")
    outfile.close()


    env=os.environ
    if "HOSTNAME" in env:
        hostname=env["HOSTNAME"]
    else:
        hostname=""
    if "scinet" in hostname:
        prefix="/home/a/aspuru/pascalf/codes/DPLL/"
    else:
        prefix="/home/pascal/Desktop/projects/boston_darpa/droplet_computer_test/paper/DPLL/"
    #command="cat sat_%s.lisp | python %s/propparse.py | python %s/cnf.py | python %s/dpll.py"%(random_string,prefix,prefix,prefix)
    command1="python %s/propparse.py sat_%s.lisp"%(prefix,random_string)
    command2="python %s/cnf.py out1_%s.dat"%(prefix,random_string)
    command3="python %s/dpll.py out2_%s.dat"%(prefix,random_string)

    args1 = shlex.split(command1)
    mystdout1 = open("out1_%s.dat"%(random_string),"a")
    process = subprocess.Popen(args1, stdout=mystdout1, stderr=subprocess.PIPE)
    out1, err1 = process.communicate()
    mystdout1.close()

    args2 = shlex.split(command2)
    mystdout2 = open("out2_%s.dat"%(random_string),"a")
    process = subprocess.Popen(args2, stdout=mystdout2, stderr=subprocess.PIPE)
    out2, err2 = process.communicate()
    mystdout2.close()

    args3 = shlex.split(command3)
    mystdout3 = open("out3_%s.dat"%(random_string),"a")
    process = subprocess.Popen(args3, stdout=mystdout3, stderr=subprocess.PIPE)
    out3, err3 = process.communicate()
    mystdout3.close()


    satisfiable=True
    outfile=open("out3_%s.dat"%(random_string),"r")
    for line in outfile:
        if "False" in line:
            satisfiable=False
            break
    outfile.close()
    os.remove("sat_%s.lisp"%(random_string))
    os.remove("out1_%s.dat"%(random_string))
    os.remove("out2_%s.dat"%(random_string))
    os.remove("out3_%s.dat"%(random_string))



    #satisfiable=False

    # Hamiltonian
    Hs=dcf.get_H_from_SAT(settings["SAT"],settings)
    #print(settings["SAT"])
    #for H_index,H in enumerate(Hs):
    #    H2=Hs2[H_index]
    #    print("rank %i"%(H_index))
    #    print(H)
    #    print(H2)
    #    print("sum deviation: %i"%(np.max(H2-H)))
    #exit()

    #print(settings["SAT"])
    if not silent:
        print("Hamiltonians:")
        for H_index,H in enumerate(Hs):
            print("\nrank %i:"%(H_index))
            print(H)
        print("\n")

    # a state counter
    if settings["check_all_states"]:
        state_counters=np.zeros((2**n))
        if 2**n>=1000:
            print("WARNING: %i bits generate %i states."%(n,2**n))
            print("         the simulation time to check all states might be long")
            time.sleep(2)
        states,energies=dcf.get_all_states(n,Hs,settings)
    else:
        states=[]
        state_counters=[]
        energies=[]

    # non-linear membrane response
    if not silent:
        dcp.plot_membrane_response(settings)

    # make an output directory
    if not os.path.exists("plots"):
        try:
            os.makedirs("plots")
        except:
            pass
    else:
        if len(os.listdir("plots"))>0:
            os.system("rm plots/*")

    # plot live
    if settings["plot"]=="live":
        plt.figure()
        plt.xticks([])
        plt.yticks([])

    # print the inital states
    dcf.print_results(states,state_counters,energies,"INITIALIZATION",Hs,settings)

    print("\n   ###   %iSAT , %i variables , %i clauses, problem is %s"%(X,n,num_clauses, satisfiable))

    num_energy_evaluations=0
    # loop over restarts
    for restart in range(settings["restarts"]):
        # random initialization of polymer and monomer
        state=2.0*np.random.random((settings["n"]))-1.0
        #state=np.array([0.3,0.6,-0.9])
        print("   ---   Annealing cycle %i out of %i"%(restart+1,settings["restarts"]))
        # loop over steps
        for step in range(settings["steps"]):

            # some debug output
            if settings["debug"]:
                print("\n   ###   Step %i out of %i\n\n"%(step+1,settings["steps"]))

            # do one time step
            dstate,noise=dcf.do_one_step(state,Hs,settings)
            mean_dstate=np.mean(np.abs(dstate))
            #print(dstate)
            if np.sum(np.abs(dstate))<0.01 and settings["noise"]<1e-8:
                print("   ---   Converged to a stable solution after %i steps (problem is %s)"%(step, satisfiable))
                break
            num_energy_evaluations+=1

            # live plot
            if settings["plot"]=="live":
                plt.imshow(state, interpolation=None, cmap='cool',vmin=-1.0, vmax=1.0)
                plt.pause(0.0001)
            # normal plot
            elif settings["plot"]=="normal":
                if settings["mode"]=="trajectory":
                    if settings["plot"]=="normal" and (step-100)%settings["plot_freq"]==0 and step>=100:
                        dcp.plot(state,settings,(step-100)//settings["plot_freq"],plot_settings,state_counters)
                elif settings["mode"]=="repeat":
                    if step+1==settings["steps"]:
                        dcp.plot(state,settings,restart,plot_settings,state_counters)

            # stop after 1000 images which is ~30 seconds of movie
            #if len(os.listdir("plots"))==1000:
            #    exit()

            # check if states counter update is requried
            change_state_counter=False
            if settings["mode"]=="trajectory": # only after some time for randomization
                if step>=2:
                    change_state_counter=True
            elif settings["mode"]=="repeat": # only in last step
                if step+1==settings["steps"]:
                    change_state_counter=True

            # update the states counter
            if change_state_counter:
                if settings["check_all_states"]:
                    stateidx=dcf.get_stateidx_from_conc(state)
                    state_counters[stateidx]+=1
                else:
                    state_here=dcf.get_state_from_conc(state)
                    if state_here in states:
                        stateidx=states.index(state_here)
                        state_counters[stateidx]+=1
                    else:
                        states.append(state_here)
                        energy_here=dcf.get_energy(Hs,state_here,settings)
                        if settings["stop_at_first_hit"] and abs(energy_here)<1e-9:
                            #print("   ---   success in step restart %i, step %i: state %s fulfulls \"%s\""%(restart,step+1,state_here,settings["SAT"]))
                            print("   ###   SUCCESS in restart %i, step %i (problem is %s)\n"%(restart,step+1,satisfiable))
                            if not silent:
                                dcf.full_output_check(state_here,settings["SAT"])
                            if not satisfiable:
                                print("###ERROR: found a solution to a non-satisfiable problem!")
                                outfile_ID=0
                                for fn in os.listdir("errors"):
                                    if "errorfile" in fn:
                                        outfile_ID+=1
                                outfile=open("errors/errorfile_%i.dat"%(outfile_ID),"w")
                                outfile.write("%iSAT , %i variables , %i clauses\n"%(X,n,num_clauses))
                                outfile.write("%s\n"%(settings["SAT"]))
                                for x in state:
                                    outfile.write("%f "%(x))
                                outfile.write("\n")
                                for x in state_here:
                                    outfile.write("%f "%(x))
                                outfile.write("\n")
                                outfile.write("Energy: %f\n"%(energy_here))
                                outfile.close()
                            return(True,satisfiable,num_energy_evaluations)
                        state_counters.append(1)
                        energies.append(energy_here)
                        if settings["plot_energy"]:
                            print("plot energy diagram in step %i"%(step))
                            
                            plt.figure()
                            plt.plot(np.array(range(len(energies)))/len(energies)*step,energies,"k-")
                            plt.xlabel("Step")
                            plt.ylabel("Energy [a.u.]")
                            plt.savefig("energy.png",dpi=90)
                            plt.close()
                            time.sleep(0.4)

            # print the results in trajectory mode after each 100 steps
            if settings["mode"]=="trajectory" and step%100==0:
                dcf.print_results(states,state_counters,energies,"RESULTS Step %i out of %i"%(step,settings["steps"]),Hs,settings)


            if settings["plot"]=="live":
                plt.savefig("plots/step_%i.png"%(step),dpi=100)
                plt.close()


        # print the results in repeat mode after each 10 annealing cycles
        if settings["mode"]=="repeat":# and restart%10==0: 
            dcf.print_results(states,state_counters,energies,"RESULTS restart %i out of %i"%(restart,settings["restarts"]),Hs,settings)

    # print the final results
    dcf.print_results(states,state_counters,energies,"FINAL RESULTS",Hs,settings)

    if settings["stop_at_first_hit"]:
        print("   ###   FAILED (problem is %s)\n"%(satisfiable))
        #if "not" not in satisfiable:
        #    print(settings["SAT"])
        #    exit()
        if satisfiable:
            outfile=open("missed/missed_SATs.dat","a")
            outfile.write("%s\n"%(settings["SAT"]))
            outfile.close()
        return(False,satisfiable,num_energy_evaluations)








if __name__== "__main__":
    run_droplet_computer()



