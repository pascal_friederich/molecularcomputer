import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse



def get_mask(N):
    mask=np.zeros((N,N))
    for i in range(N):
        for j in range(N):
            mask[i][j]=float(i)/float(N-1)
    return(mask)


def get_antiferro_hamiltonian(n_x,n_y,J):
    N=n_x*n_y
    H=np.zeros((N,N))
    for i in range(n_x):
        for j in range(n_y):
            ID1=i_j_to_ID(i,j,n_x,n_y)
            # upper, lower, left, right
            for (di,dj) in [[-1,0],[1,0],[0,-1],[0,1]]:
                i2=i+di
                j2=j+dj
                if i2>=0 and i2<=n_x-1 and j2>=0 and j2<=n_y-1:
                    ID2=i_j_to_ID(i2,j2,n_x,n_y)
                    H[ID1][ID2]=abs(J)
    return(H)


def get_ferro_hamiltonian(n_x,n_y,J):
    N=n_x*n_y
    H=np.zeros((N,N))
    for i in range(n_x):
        for j in range(n_y):
            ID1=i_j_to_ID(i,j,n_x,n_y)
            # upper, lower, left, right
            for (di,dj) in [[-1,0],[1,0],[0,-1],[0,1]]:
                i2=i+di
                j2=j+dj
                if i2>=0 and i2<=n_x-1 and j2>=0 and j2<=n_y-1:
                    ID2=i_j_to_ID(i2,j2,n_x,n_y)
                    H[ID1][ID2]=-abs(J)
    return(H)


def get_linear_chain_hamiltonian(n_x,n_y,E,J,periodic):
    if n_y!=1:
        exit("Linear chain Hamiltoian works only with n_y==1")
    N=n_x
    H=np.zeros((N,N))
    j=0
    for i in range(n_x):
        ID=i_j_to_ID(i,0,n_x,n_y)
        H[ID][ID]=E

        ID1=i_j_to_ID(i,j,n_x,n_y)
        # upper, lower
        for di in [-1,1]:
            i2=i+di
            j2=j
            if i2>=0 and i2<=n_x-1:
                ID2=i_j_to_ID(i2,j2,n_x,n_y)
                H[ID1][ID2]=J
            else:
                if periodic:
                    if i2==-1:
                        i2=n_x-1
                    if i2==n_x:
                        i2=0
                    ID2=i_j_to_ID(i2,j2,n_x,n_y)
                    print("non-periodic: %i %i , %i %i, %i ,%i"%(i,j,i2,j2,ID1,ID2))
                    H[ID1][ID2]=J
                    
    return(H)


def get_smiley_hamiltonian(E):
    es=np.zeros((7*7))
    for i in [9,11,16,18,29,33,37,38,39]:
        es[i]=E
    e_mean=np.mean(es)
    es-=e_mean
    H=np.zeros((7*7,7*7))
    for idx,e in enumerate(es):
        H[idx][idx]=e
    return(H)
