import numpy as np
import copy as cp
import cv2
from cv2.cv2 import BORDER_CONSTANT
from numpy import float16

class image_proc_cv():

    def __init__( self ):
        self.kernel3 = np.ones( ( 3, 3 ), np.uint8)

    # main access function for finding droplets in droplet array image file and 
    # computing the average pixel intensity and standard deviation for each droplet.
    # The expected number of droplets, and droplet radius is supplied as a parameter 
    # to inform algorithms and as a sanity check.       
    def droplet_intensity( self, filename, num_droplets, droplet_radius, smooth_param,  invertY=False, watershedHeight = 0.7, method="hough_watershed", marker_list=None, droplet_scale_thresh=1.0):
        
        # load the raw image into local variable bgr image stack - 3 channels of 8 bit - 24 bit image
        ori_img_bgr = cv2.imread( filename )
        
        if invertY==True:
            # flip the y direction if the invert y flag is set
            ori_img_bgr = cv2.flip(ori_img_bgr, 0)
        
        # convert to 8 bit gray scale. Always working with 8 bit image through out.
        ori_img_gray = cv2.cvtColor(ori_img_bgr, cv2.COLOR_BGR2GRAY)
        
        # rescale
        ori_img_gray = np.uint8(( 255.0 / np.max(np.float32(ori_img_gray) ) ) * np.float32(ori_img_gray))
                
        # show the original image # scaled to work with imshow (only for output during debug)
        #cv2.imshow('original image col uint8', ori_img_bgr)
        #cv2.imshow('original image gray uint8', ori_img_gray)
        #cv2.imwrite('ori img gray.png', ori_img_gray)
        
        # use a specified method to create a droplet "mask" in which each pixel
        # to be incorporated within each droplet is given a unique label.
        # The labels are ordered in the same way that a standard raster pattern 
        # through the array would encounter each labelled region.
        # Everywhere not in a droplet is labelled 0.
        num_labels, labelled_img = self.label_droplets( ori_img_bgr, ori_img_gray, num_droplets, droplet_radius, smooth_param, method=method, watershedHeight=watershedHeight, marker_list=marker_list, droplet_scale_thresh=droplet_scale_thresh)
        
        # just for debug: convert label img to uint8 and scale so we can see different labels via image
        labelled_img_show = self.computeLabelArrayForPlot(labelled_img)
        #cv2.imshow( 'labelled_droplets', labelled_img_show)
        #cv2.imwrite('labelled_droplets.png', labelled_img_show)

        # want to superimpose the labelled images as red 0 to 255
        # on original gray image in blue channel 0 to 255
        #ori_img_show_col = ori_img_bgr #cv2.cvtColor( ori_img_gray, cv2.COLOR_GRAY2RGB )
        #ori_img_show_col[:, :, 2] = 10 * labelled_img_show
        #cv2.imshow( 'superimposed', ori_img_show_col )
        #cv2.imwrite( 'superimposed.png', ori_img_show_col )
        #cv2.waitKey(0)

        # return the mean pixel intensity and standard deviation for each unique labelled region
        # works regardless of the patterns of pixels belonging to each label set. 
        return self.find_intensities(ori_img_gray, labelled_img)

    # Main decision tree. 
    # Taking the original and and gray image as input
    # this function chooses between different methods for distinguishing and labelling
    # droplets.
    # Returns an image in which there are level integer sets for 
    # each droplet with zero for background, in which the integer labels are 
    # organised according to raster pattern convention
    def label_droplets( self, ori_img_bgr, ori_img_gray, num_droplets, droplet_radius, smooth_factor, marker_list=None, method='hough_watershed', watershedHeight=0.7, droplet_scale_thresh=1.0):
        # ori_img_bgr is 3 layer uint8
        # ori_img_gray is uint16
        # bin_img is uint8
        # labelled img is uint8 - unlikely to have more than 255 droplets anytime soon.

        if method=="watershed":
            # generate a uint8 de-speckled binary image of droplets. 
            bin_img = self.binarise_and_despeckle_image( ori_img_gray )
            #cv2.imshow( 'binary image', bin_img)
            #cv2.imwrite('despeck_bin.png', bin_img)

            num_labels, img_labelled = self.label_droplets_by_watershed(ori_img_bgr, bin_img, watershedHeight=watershedHeight)
        
        elif method=="hough_circles":
            num_labels, img_labelled = self.label_droplets_by_hough(ori_img_gray, num_droplets, droplet_radius)
        
        elif method=="hough_watershed":
            # generate a uint8 binary image of droplets using the hough transform. 
            bin_img = self.hough_binarise( ori_img_gray, num_droplets, droplet_radius, smooth_factor)
            
            #cv2.imshow( 'binary image', bin_img)
            #cv2.imwrite('despeck_bin.png', bin_img)
            num_labels, img_labelled = self.label_droplets_by_watershed(ori_img_bgr, bin_img, watershedHeight=watershedHeight)

        elif method=="highpass_watershed":
            # generate a uint8 binary image of droplets using a gaussian smooth high pass filter 
            bin_img = self.highpass_binarise( ori_img_gray, droplet_radius)

            # find the marked regions.            
            num_labels, img_labelled = self.label_droplets_by_watershed(ori_img_bgr, bin_img, watershedHeight=watershedHeight)
                                
        elif method=="growmarker":

            # generate a uint8 binary image of droplets starting with a list of marker points and 
            # finding the smallest circle such that all the points of the circle are below a threshold 
            img_labelled = self.growmarker_binarise( ori_img_gray, marker_list, droplet_radius, droplet_scale_thresh=droplet_scale_thresh)
           
            num_labels = len(marker_list)
        
        elif method=="picasso":
            num_labels, img_labelled = self.picasso( ori_img_gray)
            
        else:
            print("Warning: Labelling Method Unknown. Returning correctly formatted nonsense.")
            # return a blank zero image of type uint8
            img_labelled = self.blankImage(ori_img_gray.shape, ori_img_gray[0][0])
            num_labels = 0 

        if num_labels!=num_droplets:
            # there has been a screw up. Decide what to do about it later.
            print("Warning: Num droplets found does not match num target droplets.")

        return num_labels, self.circle_sort(img_labelled, int(float(droplet_radius) * 0.7) )

    # function finds the mean and sd of the pixels in the original image
    # that correspond to regions in the labelled_img with the same label.
    # finds the highest value in the labelled_img and assumes there are that many
    # distinct labelled regions in the label_img. 
    # returns a list of (mean, sd) tuples which is the same length as the number of 
    # unique labels in the labelled img. 
    def find_intensities(self, ori_img, labelled_img):
        out_list = []
        
        # find the number of labels in the labelled_img
        num_labels = np.max(labelled_img)
        
        # loop through each label
        for label in range(1, num_labels + 1):
            # make 1D numpy array of the pixel values from the main image which
            # correspond to the regions of the labelled image with the current label. 
            vals = ori_img[labelled_img==label]
            
            #compute mean and s.d of this array of values and append it to the outlist as a tuple
            out_list.append( ( np.mean( vals ), np.std( vals ) ) )
            
        return out_list

    # function takes an image of labelled regions of integer values and relabels the img
    # in raster order. Performs a raster scan of image with coarseness "kernel size" and 
    # labels the regions in the order they are encountered, not relabelling regions that 
    # have already been encountered).
    #
    # only when over 90% of the pixels in the kernel correspond to the same value
    # do we consider that the kernal has encountered a value. This helps with irregularly shaped
    # label sets and sets that are vertically displace in an an irregular way
    
    def circle_sort( self, img, kernel_size ):
        
        # just for debug plot the original array
        #ori_img_uint8 = self.computeLabelArrayForPlot( img )
    
        # just for debug: convert label img to uint8 and scale so we can see different labels via image
        # cv2.imshow( 'labelled_droplets', ori_img_uint8)
        
        # create a copy of the original image.
        new_img = cp.copy( img )
        
        # number of labels
        max_label = np.max(img)
        
        # determine max size of image in x and y
        x_size = img.shape[ 0 ]
        y_size = img.shape[ 1 ] 
        
        # count number of kernel positions we need to check in x and y.
        kernel_x_max = int( float( img.shape[ 0 ] ) / float( kernel_size ) ) 
        kernel_y_max = int( float( img.shape[ 1 ] ) / float( kernel_size ) )
        
        # set up the new label
        new_label = np.uint8( 1 )
        
        # set up array to remember prev encountered values. Adding zero avoids relabel background
        encountered_labels = [ np.uint8( 0 ) ]  

        # set up coarse raster through image at size of kernel.
        for x in range( 0, kernel_x_max ):
            for y in range( 0, kernel_y_max ):

                # compute current range of kernel in array some checks to ensure we're not outside array. shouldn't happen.
                min_x = x * kernel_size
                if (min_x > x_size):
                    min_x = x_size - 1
                
                max_x = min_x + kernel_size
                if max_x > x_size:
                    max_x = x_size - 1

                min_y = y * kernel_size
                if min_y > y_size:
                    min_y = y_size - 1
                                
                max_y = min_y + kernel_size
                if max_y > y_size:
                    max_y = y_size - 1

                # extract sub array
                kernel = img[min_x:max_x, min_y:max_y]

                # compute histogram of kernel using numpy histogram
                hist, bins = np.histogram(kernel.ravel(), max_label + 1, [0, max_label])
                # useful debug line:
                # print(x, y, hist)
                
                # find label associated with the most popular value in the histogram
                encountered_label = np.argmax(hist)
                # if the largest value is more than 90% of the population then we consider this to be an encounter.
                # in tests the kernel became completely embedded within the droplets
                if hist[encountered_label] > 0.9 * kernel_size * kernel_size:
                    # check to see if we have encountered it before
                    if not encountered_label in encountered_labels:
                        # We have a new encounter so 
                        # relabel all the points in the new image with the new label 
                        # in places that correspond to the points in the old image
                        # with the encountered value
                        new_img[img==encountered_label] = new_label
    
                        # increment new label
                        new_label += 1
                        
                        # remember the encountered label and 
                        # ignore this value hence forth
                        encountered_labels.append(encountered_label)
            
                        # just for debug output the new copy of the array as the 
                        # algorithm advances
                        #new_img_uint8 = self.computeLabelArrayForPlot(new_img)
                        # just for debug: convert label img to uint8 and scale so we can see different labels via image
                        #cv2.imshow( str(encountered_label), new_img_uint8)
        
        return new_img

    # **** utility functions ****

    # returns a blank array of zeros the same type as the input image
    def blankImage(self, shape, var):
        return np.zeros( shape ).astype( type( var ) )

    # Spreads out the labels 0 to N into the full 255. Helps distinguish markers.
    # Any labels higher than about 10 are lost. This is purely for plotting during
    # debugging in which we assume the number of distinct labels never goes above 
    # about 10 test droplets. 
    def computeLabelArrayForPlot( self, img ):
        labelled_img_uint8 = 10 * np.uint8(img) + 20
        labelled_img_uint8[img==0] = 0
        return labelled_img_uint8


    def smooth_image(self, img, k):
        return cv2.GaussianBlur( img,(k, k), 0 )

    # generate an image from a list of circles and some image size and type information 
    def create_image_from_circles(self, shape, var, circles, radius_multiplier):
        circles = np.uint16(np.around(circles))
        # generate blank output image the same size and type as input img
        cimg = self.blankImage( shape, var)

        # create an image with each circle labelled by its index 
        # in range 1 to 65535. 0 is background
        for circ_label, circ_info in enumerate(circles[0,:]):
            # create a circle of radius 1 with line thickness of the drop radius.
            # circle is colored by circ_label an integer from 1 to 65535
            # essentially a filled circle of colour circ_label.
            # inject into cimg - a 16bit image - background is 0
            diameter =  int(2.0 * float(circ_info[2]) * radius_multiplier)
            cv2.circle(cimg, (circ_info[0],circ_info[1]), 1, [circ_label + 1, 0, 0], diameter)
        
        return circles.shape[1], cimg

    def circularise_binary(self, img):
    
        # find contour of binary image
        contours, hierarchy = cv2.findContours(img, RETR_LIST, cv2.CHAIN_APPROX_NONE)

        # generate a contour image 
        contour_img = cv2.drawContours(self.blankImage(img.shape, img[0][0]), contours, -1, (255, 255, 255), 1)

        # find a circle for each contour
        circles = [ cv2.minEnclosingCircle(contour) for contour in contours ]
        circles = np.array([np.array([ np.array( [c[0][0], c[0][1], c[1]]) for c in circles ])])
        
        # generate an image based on the circles of min enclosure
        num_circ, out_img = self.create_image_from_circles(img.shape, img[0][0], circles, 1.0)

        # create a binary image based on circles of min enclose
        ret, img_bin_2 = cv2.threshold( out_img, 0, 255, cv2.THRESH_TRIANGLE)         

        return img_bin_2

    # ***** binarise algorithms *****
        
    # function creates a binary image based on threshold values  
    # and then performs morphology opening to remove speckled noise 
    def binarise_and_despeckle_image( self, img):
        
        # smooth the image
        blur = cv2.GaussianBlur( img,(5, 5), 0 )
        
        # for debugging
        cv2.imshow('blurry',blur)
        cv2.imwrite('blurry.png',blur)
        
        # threshold the droplet array to generate a binary mask
        # may not work if different pHs are similar or lower value of gray to background.
        ret, img_bin = cv2.threshold( blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_TRIANGLE)    
        #img_bin = cv2.adaptiveThreshold( blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 0 )
        cv2.imshow('binary', img_bin)
        cv2.imwrite('binary.png', img_bin)
        
        # remove small white noise from binary image with morphology opening
        return cv2.morphologyEx( img_bin, cv2.MORPH_OPEN, self.kernel3 , iterations=2 )

    def highpass_binarise( self, img, lengthscale):
    
        # compute high pass cut off from bandpass configuration. Ensure it is odd and an integer
        hp_cutoff = int(1.5 * lengthscale)
        if hp_cutoff % 2 == 0:
            hp_cutoff += 1
        
        # set up a kernel
        #kernel = np.ones((151, 151), np.uint8)
        #kernel_2 = np.ones((51, 51), np.uint8)

        # smooth the image at the droplet lengthscale
        img_smooth = cv2.GaussianBlur(img, (hp_cutoff, hp_cutoff), 0)
        
        # open image with kernel - gets rid of boundaries
        #img_open = cv2.morphologyEx(img_smooth, cv2.MORPH_OPEN, kernel)
        
        # dilate the region as it seems to be too small
        #img_dilated = cv2.dilate(img_open, kernel_2, iterations=3)
        
        # do thresholding on smoothed, morphed, dilated image
        ret, img_bin = cv2.threshold( img_smooth, 0, 255, cv2.THRESH_TRIANGLE)         

        # take the binary image and make the isolated objects circular using min_enclosed circle.
        img_out = self.circularise_binary(img_bin)

        cv2.imshow('img', img)
        cv2.imshow('smooth', img_smooth)
        #cv2.imshow('open', img_open)
        #cv2.imshow('dilated', img_dilated)
        cv2.imshow('binarised', img_bin)
        cv2.imshow('out_img', img_out)
        cv2.waitKey(0)
        return img_out 
            
    def hough_binarise(self, ori_img, num_droplets, droplet_radius, smooth_factor):
        
        # smooth the image quite a lot.
        ori_smooth = self.smooth_image(ori_img, smooth_factor)
        #cv2.imshow( 'smoothed image', ori_smooth)
        #cv2.imwrite('smoothed hough circle image.tif', ori_smooth)
        
        # do a hough transform to find candidate markers
        numCircles, hough_circles_img = self.label_droplets_by_hough(ori_smooth, num_droplets, droplet_radius, radius_multiplier = 1.5)
        
        # generate a binary image 
        hough_circles_bin = hough_circles_img.copy()
        hough_circles_bin[hough_circles_img>0] = 255
        cv2.imshow( 'hough circle binary image', hough_circles_bin)
        cv2.imwrite('hough circle binary.tif', hough_circles_bin)

        return hough_circles_bin    
    
   
    def growmarker_binarise(self, img, marks, droplet_radius, droplet_scale_thresh=1.0):
        
        min_rad = 1 #int(0.5 * droplet_radius)
        if min_rad % 2==0:
            min_rad += 1
            
        max_rad = int(1.5 * droplet_radius)
        if max_rad % 2==0:
            max_rad -= 1
        
        out_img = self.blankImage(img.shape, img[0][0])
        
        for mark_num, mark in enumerate(marks):
            # create a new work image 
            work_img = self.blankImage(img.shape, img[0][0])
            mask_img = self.blankImage(img.shape, img[0][0])
            
            rad_vals = [a for a in range(min_rad, max_rad, 2)]
            out_vals = np.zeros(len(rad_vals)).astype(float64)
            
            # loop through radii and find the circle which has the lowest mean intensity.
            for cur_rad, radius in enumerate(rad_vals):
                
                # draw a white circle at the current radius centered at mark, thickness 2
                work_img = cv2.circle(work_img, (mark[0], mark[1]), radius, (255, 255, 255), 2)
                
                #cv2.imshow(str(radius) + 'work', work_img)
                #cv2.waitKey(0)
                
                # get the outer contour of the ever growing circle
                contour, hierarchy = cv2.findContours(work_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
                
                #draw said contour on the mask_img
                cv2.drawContours(mask_img, contour, 0, 255, 2)
                
                #cv2.imwrite('green57/'+str(radius)+'.tif', mask_img)
                
                #cv2.imshow(str(radius) + 'mask', mask_img)
                #cv2.waitKey(0)                
                #cv2.destroyAllWindows()
                # get the mean values at the mask
                out_vals[cur_rad] = float(cv2.mean(img, mask = mask_img)[0])
                
                # reset the value of the contour - got to be quicker than resetting entire image
                cv2.drawContours(mask_img, contour, 0, 0, 2)

                #print(out_vals[cur_rad])

            # for each mark find the index of lowest value in out_vals
            index_low_val = np.argmin(out_vals) 
            droplet_radius_meas = int(droplet_scale_thresh * rad_vals[index_low_val]) 
        
            # draw filled circle in the out image centered at mark with radius droplet_meas_radius
            # this generates a bin mask image with a circle of the droplet radius centered at each droplet
            # colour each droplet mark_num + 1 (background is zero.
            out_img = cv2.circle(out_img, (mark[0], mark[1]), droplet_radius_meas, (mark_num + 1, 255, 255), -1)
            
        #cv2.imshow(str(radius) + 'work', work_img)
        #cv2.waitKey(0)
        
        
        #fig, ax = plt.subplots()
        #ax.plot(out_vals)        
        #fig.canvas.manager.show()
        #fig.canvas.flush_events()
        
        #fig,ax = plt.subplots()
        #ax.plot(out_vals[0:-1] - out_vals[1:])
        #fig.canvas.manager.show()
        #fig.canvas.flush_events()
        
        #print(out_vals)
   
        return out_img
   
    def picasso(self, img):
        ''' Assumes that img is already the radon transform of the droplet (a sort of 2D projection). 
            Then attempts to use fourier slice theorem to reconstruct the original droplet, exploiting the symmetry
            of droplet.'''
        
        rows_optSize= cv2.getOptimalDFTSize(img.shape[0])
        cols_optSize= cv2.getOptimalDFTSize(img.shape[1])
        dft_size = max(cols_optSize, rows_optSize)
        imgLarge = cv2.copyMakeBorder(img, 0, dft_size - img.shape[0], 0, dft_size  - img.shape[1], cv2.BORDER_CONSTANT)
        #cv2.imshow('zeroPadded', imgLarge)
        #cv2.waitKey(0)
        
        ## generate fourier transform of slice
        #init_slice = cv2.dft(imgLarge.astype(float16))

        #rotate fourier slice 
        self.rotate_plane_image(imgLarge, 45)
        
        
    def rotate_plane_image(self, img, angle):
        ''' returns a 3D image array with the interpolated img rotated by angle 'angle' about the x axis. '''
        
        # get the longest length of the image 
        cube_length = img.shape[0] # should be an even number as getOptimalDFT size returns power of 2.
        output = np.zeros(shape=(cube_length, cube_length, cube_length)).astype(np.uint8)
        #output[:, int(cube_length/2.0), :] = img
        output[:, 0, :] = img
        reslice = output[256, :, :]
        cv2.imshow("resliced", reslice)
        cv2.waitKey(0)
        image_center = tuple(np.array(img.shape[1::-1]) / 2)
        rotMat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        cv2.imshow("resliced", output[0, :, :])
        cv2.waitKkey(0)
        output = [ cv2.warpAffine(output[a, :, :], rotMat, img.shape[1::-1], flags=cv2.INTER_LINEAR) for a in range(0, cube_length)]
        
        return output
           
       
       
   
    # ****** droplet labelling schemes ****** 
   
   
    # uses hough circle transform to identify centre and radius of each droplet
    # fills each circle a different colour, background zero and returns
    # an image of type uint8, with each circle coloured a different value.
    # also returns the number of circles in the image 
    def label_droplets_by_hough(self, img, num_circles, circle_radius, radius_multiplier=1.0):
        
        #cv2.imshow('main', img)
        #cv2.waitKey(0)
        # crudely compute the min dist to scan for droplets based on 
        # known number of droplets and size of image
        minDist = int(0.5 * float(img.shape[0])/np.sqrt(float(num_circles)))

        # find circularish regions in the image from the binary image
        circles = cv2.HoughCircles( img, 
                                    cv2.HOUGH_GRADIENT,
                                    1,
                                    minDist,
                                    param1=60, # quality of edge - smaller more circles
                                    param2=int(circle_radius * 0.7), # length of edge to be a circle
                                    minRadius=int(circle_radius * 0.8),
                                    maxRadius=int(circle_radius * 1.2))

        # round the circle information to become integers but so they can refer to pixels
        # in images larger than 255. Unlikely to getimages larger than 65536 pixels.
        if not circles is None:
            num_circles, out_img = self.create_image_from_circles(img.shape, img[0][0], circles, radius_multiplier)
        else:
            num_circles = 0
            out_img = self.blankImage( img.shape, img[0][0])
            
        img_show = self.computeLabelArrayForPlot(out_img)            
        cv2.imshow('gfg', img_show)
        cv2.waitKey(0)
        
        return num_circles, out_img

    def label_droplets_by_watershed(self, ori_img, bin_img, watershedHeight=0.7):
        
        # sure background area - also closes small holes in the fg area.
        sure_bg = cv2.dilate( bin_img, self.kernel3, iterations=3 )
        cv2.imshow( 'sure_bg', sure_bg )
        cv2.imwrite( 'sure_bg.png', sure_bg)

        # compute distance of each point from closest sure_bg pixel
        dt = cv2.distanceTransform( bin_img, cv2.DIST_L2, 5 )
        dt_norm = ( ( ( dt - dt.min() ) / (dt.max() - dt.min() ) ) * 255 ).astype(np.uint8) 
        cv2.imshow( 'distance transform', dt_norm )
        cv2.imwrite( 'distance transform.png', dt_norm )

        # determine surefg - any where more than 70% highest distance away from bg. Colour such 
        #places white.
        ret, sure_fg = cv2.threshold( dt_norm, watershedHeight * dt_norm.max(), 255, cv2.THRESH_BINARY )
        cv2.imshow( 'sure_fg', sure_fg )
        cv2.imwrite( 'sure_fg.png', sure_fg)
        
        unknown = cv2.subtract( sure_bg, sure_fg )
        cv2.imshow( 'unknown', unknown )
        cv2.imwrite( 'unknown.png', unknown)
        
        # generate labels - find all the uniquely connected regions in the sure_fg
        # labels background as 0 and each uniquely connected component with it's own label.
        ret, markers = cv2.connectedComponents( sure_fg )
        
        # the watershed algorithm takes a marker array of type int32
        # unknown regions must be marked with a 0. known and background any +ve integer
        markers = markers + 1 # add one so background is now at marker level 1
        markers[unknown==255] = 0 #set unknown to 0
        
        #markers_show = self.computeLabelArrayForPlot(markers) 
        #cv2.imshow( 'markers_unnormed', markers_show)
        #cv2.imwrite( 'markers_unnormed.png', markers_show)
        
        # generate colours for each marker
        # markers_norm = cv2.normalize(markers, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8U)
        
        # apply water shed algorithm to original image using marker colours to help
        cv2.watershed(ori_img, markers)
        ori_img[markers==-1] = [255, 0, 0]
        cv2.imshow( 'segmented image', ori_img)
        cv2.imwrite( 'segmented image.png', ori_img)
        
        # convert markers to a uint 8 with 0 in background and each region uniquely labelled
        markers = markers - 1
        markers[markers==-2] = 0
        markers_show = self.computeLabelArrayForPlot(markers) 
        cv2.imshow( 'final markers', markers_show)
        cv2.imwrite( 'final markers.png', markers_show)
                
                
        cv2.waitKey(0)
        return np.max(markers), markers

    
if __name__=='__main__':
    
    image_proc_obj = image_proc_cv()

    #red_57_filename = 'C:\Dropbox\Source\chemPU\dummy_run\images\pH_5.7_red_small.tif'
    #ori_img_bgr = cv2.imread( red_57_filename )
    #ori_img_gray = cv2.cvtColor(ori_img_bgr, cv2.COLOR_BGR2GRAY)
    #output = image_proc_obj.picasso(ori_img_gray)
    #cv2.imshow(output[1,:,:])
    

    #simple_test_fi le = "C:\Dropbox\Source\chemPU\dummy_run\images\\red_state.tif"
    #intensities_red = image_proc_obj.droplet_intensity( simple_test_file, 9, 70, 80, method="watershed")

    intensities_red = image_proc_obj.droplet_intensity( '13-03-2019_20.21.32.tif', 19, 40, 5, method="hough_watershed")
    intensities_green = image_proc_obj.droplet_intensity( '13-03-2019_20.12.10.tif', 19, 40, 5, method="hough_watershed")
    
    
    fptr = open('intensities.txt', "w")
    for i_R, i_G in intensities_red, intensities_green:
        l =  str("{:5.1f}").format(i_R[0])  + str("{:5.1f}").format(i_R[1]) + str("{:5.1f}").format(i_G[0])  + str("{:5.1f}").format(i_G[1]) + '\n'
        fptr.write(l)
    fptr.close()
    # set up filenames
    red_57_filename = 'C:\Dropbox\Source\chemPU\dummy_run\images\pH_5.7_red.tif'
    green_57_filename = 'C:\Dropbox\Source\chemPU\dummy_run\images\pH_5.7_green.tif'
    red_81_filename = 'C:\Dropbox\Source\chemPU\dummy_run\images\pH_8.1_red.tif'
    green_81_filename = 'C:\Dropbox\Source\chemPU\dummy_run\images\pH_8.1_green.tif'


    marker_list_green_57 = np.array([[514, 484]])
    marker_list_green_81 = np.array([[522, 466]])
    marker_list_red_57 = np.array([[498, 510]])
    marker_list_red_81 = np.array([[506, 482]])
    
    marker_list_green_4 = np.array([[270, 226], [1054, 224], [282, 884], [936, 966]]) 
    marker_list_red_4 = np.array([[176, 238], [964, 170], [264, 904], [928, 918]])
    
    scale_thresh_vals = [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.90, 0.95, 1.0]
    intensityVals = [None] * len(scale_thresh_vals)

    # call function that returns the mean and sd of each droplet intensity
    for i_index, s in enumerate(scale_thresh_vals):
        intensities_red = image_proc_obj.droplet_intensity( 'red.png', 4, 150, 111, invertY=True, marker_list=marker_list_red_4, droplet_scale_thresh=s, method="growmarker")
        print('red', s, intensities_red)
        intensities_green = image_proc_obj.droplet_intensity( 'green.png', 4, 150, 111, invertY=False, marker_list=marker_list_green_4, droplet_scale_thresh=s, method="growmarker")
        print('gre', s, intensities_green)
        intensities_g57 = image_proc_obj.droplet_intensity( green_57_filename, 1, 400, 111, invertY=False, marker_list=marker_list_green_57, droplet_scale_thresh=s, watershedHeight=0.7, method="growmarker")
        print('g57', s, intensities_g57)
        intensities_r57 = image_proc_obj.droplet_intensity( red_57_filename, 1, 400, 111, invertY=True, marker_list=marker_list_red_57, droplet_scale_thresh=s, watershedHeight=0.2, method="growmarker")
        print('r57', s, intensities_r57)
        intensities_g81 = image_proc_obj.droplet_intensity( green_81_filename, 1, 400, 111, invertY=False, marker_list=marker_list_green_81, droplet_scale_thresh=s, watershedHeight=0.2, method="growmarker")
        print('g81', s, intensities_g81)
        intensities_r81 = image_proc_obj.droplet_intensity( red_81_filename, 1, 400, 111, invertY=True, marker_list=marker_list_red_81, droplet_scale_thresh=s, watershedHeight=0.2, method="growmarker")
        print('r81', s, intensities_r81)
        
        intensityVals[i_index] = (intensities_red, intensities_green, intensities_g57, intensities_r57, intensities_g81, intensities_r81) 

    fptr = open('intensities.txt', "w")
    l = "s, red, gre, g57, r57, g81, r81\n"
    fptr.write(l)
    for mark_val in range(0, len(marker_list_green_4)):
        for s_index, s in enumerate(scale_thresh_vals):
            print(mark_val, s_index)
            if mark_val==0:    
                l =  str("{:3.2f}").format(s) + " " + str("{:5.1f}").format(intensityVals[s_index][0][mark_val][0]) + " " + str("{:5.1f}").format(intensityVals[s_index][1][mark_val][0]) + " " + str("{:5.1f}").format(intensityVals[s_index][2][mark_val][0]) + " " + str("{:5.1f}").format(intensityVals[s_index][3][mark_val][0]) + " " + str("{:5.1f}").format(intensityVals[s_index][4][mark_val][0]) + " " + str("{:5.1f}").format(intensityVals[s_index][5][mark_val][0]) + '\n'
            else:
                try:
                    l =  str("{:3.2f}").format(s) + " " + str("{:5.1f}").format(intensityVals[s_index][0][mark_val][0]) + " " + str("{:5.1f}").format(intensityVals[s_index][1][mark_val][0]) + '\n'
                except IndexError:
                    l = str("{:3.2f}").format(s) + "Error\n"
                
            fptr.write(l)
    
    fptr.close()
    cv2.destroyAllWindows()
    print("Done") 
    
        