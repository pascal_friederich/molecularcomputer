import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse


def plot_old(c_p,settings,step,plot_settings,nums):
    plt.figure()
    plt.imshow(c_p, interpolation=None, cmap='cool',vmin=-1.0, vmax=1.0)
    plt.xticks([])
    plt.yticks([])
    plt.colorbar()
    plt.savefig("plots/step_%i.png"%(step),dpi=100)
    plt.close()


def random_sines(step,r,n):
    s=r[0]*np.sin(0.1*step*r[1]+10.0*r[2])
    for i in range(n-1):
        s+=r[3*i+3]*np.sin(0.1*step*r[3*i+4]+10.0*r[3*i+5])
    return(s)


def logistic(x,k):
    return(1.0/(1+np.exp(-k*x)))


def plot(c_p,settings,step,plot_settings,nums):
    fig,ax=plt.subplots(subplot_kw={'aspect': 'equal'})
    #fig.patch.set_visible(False)
    ax.axis('off')
    n=c_p.shape[0]
    for i in range(n):
        r=plot_settings[i]
        polymer=c_p[i]
        polymer_0_1=0.5*(polymer+1.0)
        x=float(i)
        y=0.0
        ell=Ellipse(xy=[x,y],
                width=0.8+0.015*random_sines(step,r[0],10), height=0.8+0.015*random_sines(step,r[1],10),
                angle=10.0*random_sines(step,r[2],10),lw=2)

        ax.add_artist(ell)
        ell.set_alpha(1.0)
        ell.set_facecolor("skyblue")
        ell.set_edgecolor("k")
        num_polymers=int(round(20.0*polymer_0_1))
        num_polymers=int(round(20.0*logistic(polymer,3.0)))
        polymer_text=round((polymer_0_1*5.0))/5.0
        plt.text(i-0.49,j+0.47,"$c_{\mathrm{p}}=$%.1f"%(polymer_text),size=15)
        for k in range(num_polymers):
            start=np.array([i,j])+(np.random.random(2)-0.5)*0.5
            end=start+(np.random.random(2)-0.5)*0.1
            plt.plot([start[0],end[0]],[start[1],end[1]],"k-")
    state=[int(round(0.5*(1.0+c_p[0]))),int(round(0.5*(1.0+c_p[1]))),int(round(0.5*(1.0+c_p[2])))]
    plt.text(-0.5,0.7,"State: %i%i%i"%(state[0],state[1],state[2]),size=30)
    states=["000","001","010","011","100","101","110","111"]
    colors=["r","r","g","g","g","r","g","g"]
    for counter,num in enumerate(nums):
        plt.text(-0.5,-0.6-0.15*float(counter),"State: %s:"%(states[counter]),size=20,color=colors[counter])
        plt.text(0.6,-0.6-0.15*float(counter),"%i"%(num),size=20,color="k")
    
    #plt.imshow(c_p, interpolation=None, cmap='cool',vmin=-1.0, vmax=1.0)
    ax.set_xlim([-0.5,n-0.5])
    ax.set_ylim([-0.5,n-0.5])
    plt.gcf().subplots_adjust(bottom=0.35)
    plt.xticks([])
    plt.yticks([])
    #plt.colorbar()
    plt.savefig("plots/step_%i.png"%(step),dpi=300)
    plt.close()


def plot_membrane_response(settings):
    plt.figure()
    x_s=np.linspace(-1.0,1.0,1000)
    from droplet_computer.functions import membrane_response
    y_s= membrane_response(x_s,settings)
    plt.plot(x_s,y_s,"k-")
    plt.xlim([-1.0,1.0])
    plt.ylim([0.0,1.0])
    plt.xlabel("Input concentration")
    plt.ylabel("Output response")
    plt.savefig("membrane_response.png",dpi=150)
    plt.close()



