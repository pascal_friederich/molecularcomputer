import os
import sys
import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from itertools import permutations
from itertools import combinations
from itertools import combinations_with_replacement
from itertools import product
import copy

def full_output_check(state,SAT):
    def print_line(entries,spaces,start,end):
        line=""
        for x in entries:
            x_string="%s"%(str(x))
            line+="%s%s"%(x_string," "*(spaces-len(x_string)))
        line=start+line+end
        print(line)

    def check_clause(state,indeces,signs):
        result=False
        for idx,i in enumerate(indeces):
            if state[i]==-1 and signs[idx]==1:
                return(True)
            elif state[i]==1 and signs[idx]==-1:
                return(True)
        return(result)

    spaces=4

    state_01=[]
    for x in state:
        if x==-1:
            state_01.append(0)
        else:
            state_01.append(1)
    
    conditions=SAT.split(",")
    for cond_idx,condition in enumerate(conditions):
        indeces,signs=condition_to_parts(condition)
        to_print=[]
        for i in range(len(state)):
            if i in indeces:
                if signs[indeces.index(i)]==1:
                    to_print.append("N")
                else:
                    to_print.append("Y")
            else:
                to_print.append("")

        start_string="%i"%(cond_idx+1)
        start_string="%s%s"%(start_string," "*(spaces-len(start_string)))
        answer=check_clause(state,indeces,signs)
        print_line(state_01,spaces," "*(len(start_string)),"")
        print_line(to_print,spaces,start_string,str(answer))


def generate_random_clause(n,X,index_must=None):
    if index_must==None:
        numbers=np.sort(np.random.choice(n, size=X, replace=False))
    else:
        numbers=[]
        while index_must not in numbers:
            numbers=np.sort(np.random.choice(n, size=X, replace=False))
    nots=np.random.choice(2, size=X, replace=True)
    strings=[]
    for idx,num in enumerate(numbers):
        not_here=nots[idx]
        if not_here==0:
            strings.append("%i"%(num+1))
        else:
            strings.append("not%i"%(num+1))
    clause=strings[0]
    for s in strings[1:]:
        clause+=" or %s"%(s)
    return(clause)

def sat_from_clauses(clauses):
    sat=clauses[0]
    for c in clauses[1:]:
        sat+=" , %s"%(c)
    return(sat)

def list_product(l):
    p = 1.0
    for i in l:
        p *= i
    return(p)




def get_H_from_SAT(SAT,settings):
    n=settings["n"]
    X=settings["X"]
    Hs=[0.0]
    for x in range(1,X+1):
        shape=[]
        for i in range(x):
            shape.append(n)
        H=np.zeros(shape)
        Hs.append(H)

    conditions=SAT.split(",")
    for condition in conditions:
        indeces,signs=condition_to_parts(condition)
        #print("condition ",condition, indeces,signs)
        # rank 0
        Hs[0]+=1.0/float(2.0**len(indeces))
        # rank 1
        for index in range(len(indeces)):
            Hs[1][indeces[index]]+=signs[index]/float(2.0**len(indeces))
        # rank 2 to X
        for rank in range(2,X+1):
            #print("add H of rank %i"%(rank))
            # find all subsets of indeces that have length "rank"
            subsets=[x for x in combinations(range(len(indeces)),rank)]

            for subset in subsets:
                indeces_subset=[]
                signs_subset=[]
                for i in subset:
                    indeces_subset.append(indeces[i])
                    signs_subset.append(signs[i])
                #print("  use subset with signs ",indeces_subset,signs_subset)
                # find all permutations of these indeces
                perms = set(permutations(range(len(indeces_subset))))
                for perm in perms:
                    indeces_perm=[]
                    signs_perm=[]
                    for i in perm:
                        indeces_perm.append(indeces_subset[i])
                        signs_perm.append(signs_subset[i])
                    indeces_perm=tuple(indeces_perm)
                    # get the product of all signs of the current permutation
                    sign_perm=list_product(signs_perm)
                    #print("    use permutation with sign ",indeces_perm,sign_perm)
                    # update the hamiltonian according to the current permutation
                    Hs[rank][indeces_perm]+=sign_perm/float(2.0**len(indeces))/float(len(perms))
    return(Hs)

def condition_to_parts(condition):
    indeces=[]
    signs=[]
    for part_idx,part in enumerate(condition.split()):
        if part_idx%2==0:
            i=int(part.replace("not",""))-1
            if "not" in part:
                sign_i=1
            else:
                sign_i=-1
            indeces.append(i)
            signs.append(sign_i)
    return([indeces,signs])

def get_energy(Hs,state,settings):
    n=settings["n"]
    energy=Hs[0]
    #energies=[energy]
    for H_index,H in enumerate(Hs):
        if H_index>=1:
            #energy_H=0.0
            if settings["debug"]:
                print("get energy from Hamiltonian with rank %i"%(H_index))
            ijk_combinations=combinations(range(n),H_index)
            if settings["debug"]:
                ijk_combinations_list=list(copy.copy(ijk_combinations))
                print("found %i ijk combinations of length %i"%(len(ijk_combinations_list),len(ijk_combinations_list[0])))
                print(ijk_combinations_list)

            for ijk_combination in ijk_combinations:
                energy_here=H[ijk_combination]*state_product(state,ijk_combination)
                energy+=energy_here*np.math.factorial(H_index)
                #energy_H+=energy_here*np.math.factorial(H_index)
            #energies.append(energy_H)
    # make -0.0 to 0.0
    if abs(energy)<1e-9 and energy<0.0:
        energy=abs(energy)
    #print(energies)
    return(energy)


def get_energy_non_reduced(Hs,state,settings):
    n=settings["n"]
    energy=Hs[0]
    #energies=[energy]
    for H_index,H in enumerate(Hs):
        if H_index>=1:
            #energy_H=0.0
            if settings["debug"]:
                print("get energy from Hamiltonian with rank %i"%(H_index))
            ijk_combinations=combinations_with_replacement(range(n),H_index)
            if settings["debug"]:
                ijk_combinations_list=list(copy.copy(ijk_combinations))
                print("found %i ijk combinations of length %i"%(len(ijk_combinations_list),len(ijk_combinations_list[0])))
                print(ijk_combinations_list)

            for ijk_combination in ijk_combinations:
                energy_here=H[ijk_combination]*state_product(state,ijk_combination)

                permuts=set([x for x in permutations(ijk_combination)])
                num_permuts=len(permuts)
                energy+=energy_here*float(num_permuts)#np.math.factorial(H_index)
                #energy_H+=energy_here*np.math.factorial(H_index)
            #energies.append(energy_H)
    # make -0.0 to 0.0
    if abs(energy)<1e-9 and energy<0.0:
        energy=abs(energy)
    #print(energies)
    return(energy)

'''
def get_energy_01_3SAT(Hs,state,settings):
    n=settings["n"]
    X=settings["X"]
    energy=Hs[0]
    energies=[energy]
    for H_index,H in enumerate(Hs):
        if H_index>=1:
            energy_H=0.0
            if settings["debug"]:
                print("get energy from Hamiltonian with rank %i"%(H_index))
            ijk_combinations=product(range(n),repeat=H_index)
            if settings["debug"]:
                ijk_combinations_list=list(copy.copy(ijk_combinations))
                print("found %i ijk combinations of length %i"%(len(ijk_combinations_list),len(ijk_combinations_list[0])))
                print(ijk_combinations_list)

            for ijk_combination in ijk_combinations:
                energy_here=H[ijk_combination]*state_product(state,ijk_combination)
                if H_index==2:
                    energy_here*=3.0
                energy+=energy_here#/np.math.factorial(H_index)
                energy_H+=energy_here#/np.math.factorial(H_index)
            energies.append(energy_H)
    # make -0.0 to 0.0
    if abs(energy)<1e-9 and energy<0.0:
        energy=abs(energy)
    #print(energies)
    return(energy)


def get_energy_01_4SAT(Hs,state,settings):
    n=settings["n"]
    X=settings["X"]
    energy=Hs[0]
    energies=[energy]
    for H_index,H in enumerate(Hs):
        if H_index>=1:
            energy_H=0.0
            if settings["debug"]:
                print("get energy from Hamiltonian with rank %i"%(H_index))
            ijk_combinations=product(range(n),repeat=H_index)
            if settings["debug"]:
                ijk_combinations_list=list(copy.copy(ijk_combinations))
                print("found %i ijk combinations of length %i"%(len(ijk_combinations_list),len(ijk_combinations_list[0])))
                print(ijk_combinations_list)

            for ijk_combination in ijk_combinations:
                energy_here=H[ijk_combination]*state_product(state,ijk_combination)
                if H_index==2:
                    energy_here*=7.0
                elif H_index==3:
                    energy_here*=6.0
                energy+=energy_here#/np.math.factorial(H_index)
                energy_H+=energy_here#/np.math.factorial(H_index)
            energies.append(energy_H)
    # make -0.0 to 0.0
    if abs(energy)<1e-9 and energy<0.0:
        energy=abs(energy)
    #print(energies)
    return(energy)
'''

def get_all_states(n,Hs,settings):
    num=2**n
    all_states=[]
    all_energies=[]
    for i in range(num):
        state_here=[]
        state_here_string="{0:b}".format(i)
        if len(state_here_string)<n:
            state_here_string="%s%s"%("0"*(n-len(state_here_string)),state_here_string)
        for char in state_here_string:
            if char=="0":
                state_here.append(-1)
            elif char=="1":
                state_here.append(1)
        all_states.append(state_here)
        energy=get_energy(Hs,state_here,settings)
        all_energies.append(energy)
    return(all_states,all_energies)


def check_if_satisfiable_brute_force(n,Hs,settings):
    num_energy_evaulations=0
    num=2**n
    all_states=[]
    all_energies=[]
    for i in range(num):
        state_here=[]
        state_here_string="{0:b}".format(i)
        if len(state_here_string)<n:
            state_here_string="%s%s"%("0"*(n-len(state_here_string)),state_here_string)
        for char in state_here_string:
            if char=="0":
                state_here.append(-1)
            elif char=="1":
                state_here.append(1)
        all_states.append(state_here)
        energy=get_energy(Hs,state_here,settings)
        num_energy_evaulations+=1
        all_energies.append(energy)
        if abs(energy)<1e-6:
            return(True,num_energy_evaulations)
    return(False,num_energy_evaulations)



def membrane_response(x,settings):
    # a non-linear function from 0 to 1 on an interval from 0 to 1
    if settings["membrane"]=="linear":
        y=0.5*(x+1.0)
    elif settings["membrane"]=="quadratic":
        y=0.25*(x+1.0)**2.0
    #y=1.0/(2.0**0.5)*(x+1.0)**0.5
    return(y)


def i_j_to_ID(i,j,n_x,n_y):
    return(i*n_y + j)


def ID_to_i_j(ID,n_x,n_y):
    i=ID // n_y
    j=ID % n_y
    return(i,j)


def state_product(state,new_indeces):
    prod=1.0
    for idx in new_indeces:
        prod*=state[idx]
    return(prod)




def do_one_step(state,Hs,settings):
    # number of sites
    n=settings["n"]

    ########################
    # 1) Get the gradients #
    ########################

    # diagonal values
    dstate=np.zeros((settings["n"]))
    for i in range(n):
        ds=Hs[1][i]
        dstate[i]+=ds
  
    if settings["debug"]:
        print("state:   %s"%(state))
        print("gradients after diagonal elements")
        print("dstate:   %s"%(dstate))


    # off-diagonal values
    #print(Hs[2])
    for i in range(n):
        if settings["debug"]:
            print("get gradients for droplet %i"%(i))
        for H_index,H in enumerate(Hs):
            if H_index>=2:
                if settings["debug"]:
                    print("get gradients from Hamiltonian with rank %i"%(H_index))
                jk_combinations=combinations(range(n),H_index-1)
                if settings["debug"]:
                    jk_combinations_list=list(copy.copy(jk_combinations))
                    print("found %i jk combinations of length %i"%(len(jk_combinations_list),len(jk_combinations_list[0])))
                    print(jk_combinations_list)

                for jk_combination in jk_combinations:
                    ijk_combination=tuple([i]+list(jk_combination))
    
                    dstate_here=H[ijk_combination]*state_product(state,jk_combination)*2.0*np.math.factorial(H_index-1)
                    dstate[i]+=dstate_here
                    #if i==0:
                    #    print("ijk-combination: ",ijk_combination)
                    #    print("state: %s"%(state))
                    #    print("H[ijk]: %f"%(H[ijk_combination]))
                    #    print("product(state[jk]: %f"%(state_product(state,jk_combination)))
                    #    print("d_state_here: %f"%(dstate_here))
                    #    print("final dstate[0]: %f"%(dstate[i]))


    ##################################################
    # 2) change the state according to the gradients #
    ##################################################

    if settings["debug"]:
        print("final gradients")
        print("dstate:               %s"%(dstate))
        print("appy changes to state %s"%(state))


    dstate_corrected=[]
    remaining=0
    for i,d in enumerate(dstate):
        if d>0.0 and state[i]<(-1.0+1e-8):
            dstate_corrected.append(0.0)
        elif d<0.0 and state[i]>(1.0-1e-8):
            dstate_corrected.append(0.0)
        else:
            dstate_corrected.append(d)
            remaining+=1
    #print(remaining)
    dstate_corrected=np.array(dstate_corrected)

    mean_abs_dstate=np.mean(np.abs(dstate_corrected))
    #norm_dstate=np.linalg.norm(dstate_corrected)
    sum_abs_dstate=np.sum(np.abs(dstate_corrected))
    
    if sum_abs_dstate>1e-10 and remaining>0:
        normalization=sum_abs_dstate/float(remaining)
        dstate_normalized=np.copy(dstate_corrected)/normalization
    else:
        dstate_normalized=np.copy(dstate_corrected)



    noise_weights=np.ones((n))
    #for i in range(n):
    #    if i not in [0,1,2,3,4,5,6]:
    #        noise_weights[i]=20.0

    noise=np.random.normal(size=n,loc=np.zeros((n)),scale=noise_weights*settings["noise"])
    for i in range(n):
        # state change is proportional to dstate and state itself
        dstate_real=-dstate_normalized[i]*settings["stepsize"]
        dstate_real_noise=dstate_real+noise[i]
        state[i]+=dstate_real_noise

        if state[i]>1.0:
            state[i]=1.0
            #dstate[i]=0.0
        elif state[i]<-1.0:
            state[i]=-1.0
            #dstate[i]=0.0

    '''
    # flip noise
    if norm_dstate<1e-8:
        probability_first=1e-3
        probability_second=.01
        change_done=False
        for i in range(7):
            if np.random.random()<probability_first:
                print("flipped bit %i from %i to %i"%(i,state[i],-state[i]))
                state[i]*=-1
                change_done=True
        if change_done:
            for i in range(7,n):
                if np.random.random()<probability_second:
                    print("flipped bit %i from %i to %i"%(i,state[i],-state[i]))
                    state[i]*=-1
        if change_done:
            os.system("sleep 0.4")
    '''

    if settings["debug"]:
        print("new state:            %s"%(state))


    return(dstate_normalized,noise)



def do_one_step_protein(state,Hs,settings, freeze_first_seven=False):
    # number of sites
    n=settings["n"]

    ########################
    # 1) Get the gradients #
    ########################

    # diagonal values
    dstate=np.zeros((settings["n"]))
    for i in range(n):
        ds=Hs[1][i]
        dstate[i]+=ds
  
    if settings["debug"]:
        print("state:   %s"%(state))
        print("gradients after diagonal elements")
        print("dstate:   %s"%(dstate))


    # off-diagonal values
    #print(Hs[2])
    for i in range(n):
        if settings["debug"]:
            print("get gradients for droplet %i"%(i))
        for H_index,H in enumerate(Hs):
            if H_index>=2:
                if settings["debug"]:
                    print("get gradients from Hamiltonian with rank %i"%(H_index))
                jk_combinations=combinations(range(n),H_index-1)
                if settings["debug"]:
                    jk_combinations_list=list(copy.copy(jk_combinations))
                    print("found %i jk combinations of length %i"%(len(jk_combinations_list),len(jk_combinations_list[0])))
                    print(jk_combinations_list)

                for jk_combination in jk_combinations:
                    ijk_combination=tuple([i]+list(jk_combination))
    
                    dstate_here=H[ijk_combination]*state_product(state,jk_combination)*2.0*np.math.factorial(H_index-1)
                    dstate[i]+=dstate_here
                    #if i==0:
                    #    print("ijk-combination: ",ijk_combination)
                    #    print("state: %s"%(state))
                    #    print("H[ijk]: %f"%(H[ijk_combination]))
                    #    print("product(state[jk]: %f"%(state_product(state,jk_combination)))
                    #    print("d_state_here: %f"%(dstate_here))
                    #    print("final dstate[0]: %f"%(dstate[i]))


    ##################################################
    # 2) change the state according to the gradients #
    ##################################################

    if settings["debug"]:
        print("final gradients")
        print("dstate:               %s"%(dstate))
        print("appy changes to state %s"%(state))


    if freeze_first_seven:
        # no changes on conformation!
        for i in range(7):
            dstate[i]=0.0

    dstate_corrected=[]
    remaining=0
    for i,d in enumerate(dstate):
        if d>0.0 and state[i]<(-1.0+1e-8):
            dstate_corrected.append(0.0)
        elif d<0.0 and state[i]>(1.0-1e-8):
            dstate_corrected.append(0.0)
        else:
            dstate_corrected.append(d)
            remaining+=1
    #print(remaining)
    dstate_corrected=np.array(dstate_corrected)

    mean_abs_dstate=np.mean(np.abs(dstate_corrected))
    #norm_dstate=np.linalg.norm(dstate_corrected)
    sum_abs_dstate=np.sum(np.abs(dstate_corrected))
    
    if sum_abs_dstate>1e-10 and remaining>0:
        normalization=sum_abs_dstate/float(remaining)
        dstate_normalized=np.copy(dstate_corrected)/normalization
    else:
        dstate_normalized=np.copy(dstate_corrected)



    noise_weights=np.ones((n))
    #for i in range(n):
    #    if i not in [0,1,2,3,4,5,6]:
    #        noise_weights[i]=20.0

    noise=np.random.normal(size=n,loc=np.zeros((n)),scale=noise_weights*settings["noise"])
    for i in range(n):
        # state change is proportional to dstate and state itself
        dstate_real=-dstate_normalized[i]*settings["stepsize"]
        dstate_real_noise=dstate_real+noise[i]
        state[i]+=dstate_real_noise

        if state[i]>1.0:
            state[i]=1.0
            #dstate[i]=0.0
        elif state[i]<-1.0:
            state[i]=-1.0
            #dstate[i]=0.0

    '''
    # flip noise
    if norm_dstate<1e-8:
        probability_first=1e-3
        probability_second=.01
        change_done=False
        for i in range(7):
            if np.random.random()<probability_first:
                print("flipped bit %i from %i to %i"%(i,state[i],-state[i]))
                state[i]*=-1
                change_done=True
        if change_done:
            for i in range(7,n):
                if np.random.random()<probability_second:
                    print("flipped bit %i from %i to %i"%(i,state[i],-state[i]))
                    state[i]*=-1
        if change_done:
            os.system("sleep 0.4")
    '''

    if settings["debug"]:
        print("new state:            %s"%(state))


    return(dstate_normalized,noise)





def do_one_step_protein_non_reduced(state,Hs,settings, freeze_first_seven=False):
    # number of sites
    n=settings["n"]

    time0=time.time()
    ########################
    # 1) Get the gradients #
    ########################

    # diagonal values
    dstate=np.zeros((settings["n"]))
    for i in range(n):
        ds=Hs[1][i]
        dstate[i]+=ds
  
    if settings["debug"]:
        print("state:   %s"%(state))
        print("gradients after diagonal elements")
        print("dstate:   %s"%(dstate))


    # off-diagonal values
    #print(Hs[2])
    for i in range(n):
        if settings["debug"]:
            print("get gradients for droplet %i"%(i))
        for H_index,H in enumerate(Hs):
            if H_index>=2:
                if settings["debug"]:
                    print("get gradients from Hamiltonian with rank %i"%(H_index))
                jk_combinations=combinations_with_replacement(range(n),H_index-1)
                if settings["debug"]:
                    jk_combinations_list=list(copy.copy(jk_combinations))
                    print("found %i jk combinations of length %i"%(len(jk_combinations_list),len(jk_combinations_list[0])))
                    print(jk_combinations_list)

                for jk_combination in jk_combinations:
                    ijk_combination=tuple([i]+list(jk_combination))
    
                    if abs(H[ijk_combination])>1e-3:

                        setlength=len(set(jk_combination))

                        num_is=ijk_combination.count(i)

                        permuts=set([x for x in permutations(ijk_combination)])
                        num_permuts=len(permuts)

                        dstate_here=H[ijk_combination]*float(num_is)*state_product(state,jk_combination)*float(num_permuts)#np.math.factorial(setlength)
                        #if abs(H[ijk_combination])>0.000001:
                        #    print("the gradient of %s is %f*%i*%i*2.0*%i = %f"%(str(ijk_combination),H[ijk_combination],num_is,state_product(state,jk_combination),num_permuts,dstate_here))
                        dstate[i]+=dstate_here
                        #if i==0:
                        #    print("ijk-combination: ",ijk_combination)
                        #    print("state: %s"%(state))
                        #    print("H[ijk]: %f"%(H[ijk_combination]))
                        #    print("product(state[jk]: %f"%(state_product(state,jk_combination)))
                        #    print("d_state_here: %f"%(dstate_here))
                        #    print("final dstate[0]: %f"%(dstate[i]))

    time1=time.time()
    time_deriv1=time1-time0
    #time0=time.time()
    ## numerical derivative to compare
    #dstate_numerical=np.zeros((settings["n"]))
    #epsilon=0.00001
    #for i in range(n):
    #    state_1=np.copy(state)
    #    state_1[i]-=epsilon
    #    state_2=np.copy(state)
    #    state_2[i]+=epsilon
    #    dstate_numerical[i]=(get_energy_non_reduced(Hs,state_2,settings)-get_energy_non_reduced(Hs,state_1,settings))/(2.0*epsilon)
    ##print(dstate_numerical)
    #time1=time.time()
    #time_deriv2=time1-time0
    #print(time_deriv1,time_deriv2)

    #exit()
    ##################################################
    # 2) change the state according to the gradients #
    ##################################################

    if settings["debug"]:
        print("final gradients")
        print("dstate:               %s"%(dstate))
        print("appy changes to state %s"%(state))


    if freeze_first_seven:
        # no changes on conformation!
        for i in range(7):
            dstate[i]=0.0

    dstate_corrected=[]
    remaining=0
    for i,d in enumerate(dstate):
        if d>0.0 and state[i]<(-1.0+1e-8):
            dstate_corrected.append(0.0)
        elif d<0.0 and state[i]>(1.0-1e-8):
            dstate_corrected.append(0.0)
        else:
            dstate_corrected.append(d)
            remaining+=1
    #print(remaining)
    dstate_corrected=np.array(dstate_corrected)

    mean_abs_dstate=np.mean(np.abs(dstate_corrected))
    #norm_dstate=np.linalg.norm(dstate_corrected)
    sum_abs_dstate=np.sum(np.abs(dstate_corrected))
    
    if sum_abs_dstate>1e-10 and remaining>0:
        normalization=sum_abs_dstate/float(remaining)
        dstate_normalized=np.copy(dstate_corrected)/normalization
    else:
        dstate_normalized=np.copy(dstate_corrected)



    noise_weights=np.ones((n))
    #for i in range(n):
    #    if i not in [0,1,2,3,4,5,6]:
    #        noise_weights[i]=20.0


    noise=np.random.normal(size=n,loc=np.zeros((n)),scale=noise_weights*settings["noise"])
    for i in range(n):
        # state change is proportional to dstate and state itself
        dstate_real=-dstate_normalized[i]*settings["stepsize"]
        dstate_real_noise=dstate_real+noise[i]
        state[i]+=dstate_real_noise
        if state[i]>1.0:
            state[i]=1.0
            #dstate[i]=0.0
        elif state[i]<-1.0:
            state[i]=-1.0
            #dstate[i]=0.0

    '''
    # flip noise
    if norm_dstate<1e-8:
        probability_first=1e-3
        probability_second=.01
        change_done=False
        for i in range(7):
            if np.random.random()<probability_first:
                print("flipped bit %i from %i to %i"%(i,state[i],-state[i]))
                state[i]*=-1
                change_done=True
        if change_done:
            for i in range(7,n):
                if np.random.random()<probability_second:
                    print("flipped bit %i from %i to %i"%(i,state[i],-state[i]))
                    state[i]*=-1
        if change_done:
            os.system("sleep 0.4")
    '''

    if settings["debug"]:
        print("new state:            %s"%(state))

    #print(dstate_normalized)
    #print(state)
    #exit()    
    return(dstate_normalized,noise)



def get_forces(state, Hs, settings):

    # number of sites
    n=settings["n"]

    time0=time.time()
    ########################
    # 1) Get the gradients #
    ########################

    # diagonal values
    dstate=np.zeros((settings["n"]))
    for i in range(n):
        ds=Hs[1][i]
        dstate[i]+=ds
  
    if settings["debug"]:
        print("state:   %s"%(state))
        print("gradients after diagonal elements")
        print("dstate:   %s"%(dstate))

    # off-diagonal values
    #print(Hs[2])
    for i in range(n):
        if settings["debug"]:
            print("get gradients for droplet %i"%(i))
        for H_index,H in enumerate(Hs):
            if H_index>=2:
                if settings["debug"]:
                    print("get gradients from Hamiltonian with rank %i"%(H_index))
                jk_combinations=combinations_with_replacement(range(n),H_index-1)
                if settings["debug"]:
                    jk_combinations_list=list(copy.copy(jk_combinations))
                    print("found %i jk combinations of length %i"%(len(jk_combinations_list),len(jk_combinations_list[0])))
                    print(jk_combinations_list)

                for jk_combination in jk_combinations:
                    ijk_combination=tuple([i]+list(jk_combination))
    
                    if abs(H[ijk_combination])>1e-3:

                        setlength=len(set(jk_combination))

                        num_is=ijk_combination.count(i)

                        permuts=set([x for x in permutations(ijk_combination)])
                        num_permuts=len(permuts)

                        dstate_here=H[ijk_combination]*float(num_is)*state_product(state,jk_combination)*float(num_permuts)#np.math.factorial(setlength)
                        #if abs(H[ijk_combination])>0.000001:
                        #    print("the gradient of %s is %f*%i*%i*2.0*%i = %f"%(str(ijk_combination),H[ijk_combination],num_is,state_product(state,jk_combination),num_permuts,dstate_here))
                        dstate[i]+=dstate_here

    return(dstate)

def do_one_step_protein_non_reduced_MD(state, Hs, trajectory, settings, freeze_first_seven=False, fixed_indeces=[]):

    thermostat="B" # "NH" or "B"

    n=settings["n"]
    ########################
    # 1) Get the gradients #
    ########################
    #dstate=get_forces(state)

    ##################################################
    # 2) change the state according to the gradients #
    ##################################################

    if settings["debug"]:
        print("final gradients")
        print("dstate:               %s"%(dstate))
        print("appy changes to state %s"%(state))


    # Verlet algorithm
    # 1) rescale p with thermostat
    # 2) p=p+0.5*dt*f
    # 3) r=r+dt*p/m
    # 4) f=force(r)
    # 5) p=p+0.5*dt*f


    if len(trajectory["momenta"])>0:
        momentum_old=trajectory["momenta"][-1]
    else:
        momentum_old=np.random.random((n))
        for idx in fixed_indeces:
            momentum_old[idx]=0.0
        temperature_here=2.0/(2.0*float(n))*0.5*np.sum(momentum_old**2.0)
        scaling_factor=(settings["temperature"]/temperature_here)**0.5
        momentum_old*=scaling_factor
    if len(trajectory["forces"])>0:
        force_old=trajectory["forces"][-1]
    else:
        force_old=np.zeros((n))
    if len(trajectory["Ss"])>0:
        S_old=trajectory["Ss"][-1]
    else:
        S_old=0.0
    if settings["debug"]:
        print("momentum_old: ", momentum_old)
    stepnumber=len(trajectory["momenta"])

    # 1) Thermostat
    dimension=1.0
    degrees_of_freedom=dimension*(float(n)-float(len(fixed_indeces)))
    if thermostat=="NH":

        if stepnumber<30:
            # Berendsen thermostat
            temperature_here=2.0/degrees_of_freedom*0.5*np.sum(momentum_old**2.0)
            #coupling=1.0
            #scaling_factor=(1.0+settings["stepsize"]/coupling*(settings["temperature"]/()-1.0))**0.5
            scaling_factor=(settings["temperature"]/temperature_here)**0.5
            momentum_old*=scaling_factor
            temperature_after_scaling=2.0/degrees_of_freedom*0.5*np.sum(momentum_old**2.0)
            print("temperature: %f , temperature needed: %f, scaling factor: %f, temperature after scaling: %f"%(temperature_here,settings["temperature"],scaling_factor,temperature_after_scaling))
            S=0.0
        else:
            # Nose-Hoover thermostat
            print("NOSE-HOOVER THERMOSTAT")
            temperature_here=2.0/degrees_of_freedom*0.5*np.sum(momentum_old**2.0)
            print("temperature: %f , temperature needed: %f"%(temperature_here,settings["temperature"]))
            M_S=0.1
            dS=1.0/M_S*(0.5*np.sum(momentum_old**2.0)-0.5*degrees_of_freedom*settings["temperature"])
            if temperature_here>settings["temperature"]:
                print("temperature is too large, change S by %f"%(dS))
            else:
                print("temperature is too small, change S by %f"%(dS))
            S=S_old+dS*settings["stepsize"]
            #if settings["debug"]:
            #if S>0.5:
            #    S=0.5
            #elif S<-0.05:
            #    S=-0.05
            print("S old: ",S_old, "S new: ",S)
            momentum_old*=(1.0-S*settings["stepsize"])
            print("scaling factor for momenta is %f"%(1.0-S))
            temperature_after_scaling=2.0/degrees_of_freedom*0.5*np.sum(momentum_old**2.0)
            print("temperature after scaling: %f"%(temperature_after_scaling))

    elif thermostat=="B":
        # Berendsen thermostat
        temperature_here=2.0/degrees_of_freedom*0.5*np.sum(momentum_old**2.0)
        #coupling=1.0
        #scaling_factor=(1.0+settings["stepsize"]/coupling*(settings["temperature"]/()-1.0))**0.5
        scaling_factor=(settings["temperature"]/temperature_here)**0.5
        momentum_old*=scaling_factor
        temperature_after_scaling=2.0/degrees_of_freedom*0.5*np.sum(momentum_old**2.0)
        print("temperature: %f , temperature needed: %f, scaling factor: %f, temperature after scaling: %f"%(temperature_here,settings["temperature"],scaling_factor,temperature_after_scaling))
        S=0.0

    # 2) Momentum update I
    momentum_new = momentum_old - 0.5*settings["stepsize"]*force_old
    if settings["debug"]:
        print("momentum_new: ",momentum_new)

    # 3) State update
    if settings["debug"]:
        print("state old:")
        print(state)
    if len(fixed_indeces)>0:
        changes_to_fixed_states=np.array(momentum_new)[np.array(fixed_indeces)]
        if np.max(np.abs(changes_to_fixed_states))>1e-9:
            exit("ERROR: TRY TO CHANGE A FIXED STATE")
    for i in range(n):
        dstate=momentum_new[i]*settings["stepsize"]
        state[i]+=dstate
        if state[i]>1.0:
            state[i]=1.0
            # set the momentum to zero to get the correct temperatures in the next step
            momentum_old[i]=0.0
            momentum_new[i]=0.0
        elif state[i]<-1.0:
            state[i]=-1.0
            # set the momentum to zero to get the correct temperatures in the next step
            momentum_old[i]=0.0
            momentum_new[i]=0.0


    if settings["debug"]:
        print("state new:")
        print(state)

    # 4) Force calculation
    force_new=get_forces(state, Hs, settings)
    if settings["debug"]:
        print("forces: ")
        print(force_new)

    noise=np.random.normal(size=n,loc=np.zeros((n)),scale=settings["noise"])
    if settings["debug"]:
        print("noise:")
        print(noise)
    force_new+=noise

    freeze_first_seven=False
    if freeze_first_seven:
        # no changes on conformation!
        for i in range(7):
            force_new[i]=0.0
    # freeze the frozen indeces
    for idx in fixed_indeces:
        force_new[idx]=0.0
    force_new_corrected=[]
    remaining=0
    #max_grad=0.01
    for i,f in enumerate(force_new):
        if f>0.0 and state[i]<(-1.0+1e-8):
            force_new_corrected.append(0.0)
        elif f<0.0 and state[i]>(1.0-1e-8):
            force_new_corrected.append(0.0)
        #elif f>max_grad:
        #    force_new_corrected.append(max_grad)
        #    remaining+=1
        #elif f<-max_grad:
        #    force_new_corrected.append(-max_grad)
        #    remaining+=1
        else:
            force_new_corrected.append(f)
            remaining+=1
    force_new_corrected=np.array(force_new_corrected)
    if settings["debug"]:
        print("forces corrected: ")
        print(force_new_corrected)

    # 5) Momentum update II
    momentum_new = momentum_new - 0.5*settings["stepsize"]*force_new_corrected

    trajectory["momenta"].append(momentum_new)
    trajectory["forces"].append(force_new)
    trajectory["Ss"].append(S)
    trajectory["temperatures"].append(temperature_after_scaling)

    return(noise)






def print_results(states,state_counters,energies,caption,Hs,settings):
    if not settings["silent"]:
        print(caption)
        if len(state_counters)>0:
            max_len=max([len(str(i)) for i in state_counters])
        for stateidx,state in enumerate(states):
            energy=energies[stateidx]
            state_string=""
            for x in state:
                state_string+="%i"%((x+1)/2)
            #if energy<-1e-9:
            #    exit("ERROR: zero energy detected for state %s. exit."%(state_string))
            state_count=state_counters[stateidx]
            state_count_str="%i%s"%(state_count," "*(max_len-len(str(state_count))))
            if energy<1e-9:
                print("%s : %s , energy: %.2f (good)"%(state_string,state_count_str,energy))
            else:
                print("%s : %s , energy: %.2f "%(state_string,state_count_str,energy))

def get_state_from_conc(c):
    state_here=[]
    for conc in c:
        state_here.append(2*int(round(0.5*(1.0+conc)))-1)
    return(state_here)

def get_stateidx_from_conc(c):
    # old: int(round(0.5*(1.0+c_p[2][0])))+2*int(round(0.5*(1.0+c_p[1][0])))+2*2*int(round(0.5*(1.0+c_p[0][0])))
    N=len(c)
    stateidx=0
    for idx in range(N):
        stateidx+=round(0.5*(1.0+c[N-1-idx]))*(2**idx)
    return(int(stateidx))



